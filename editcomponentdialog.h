#ifndef EDITCOMPONENTDIALOG_H
#define EDITCOMPONENTDIALOG_H

#include <QDialog>

namespace Ui {
class EditComponentDialog;
}

class EditComponentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditComponentDialog(QWidget *parent = 0);
    ~EditComponentDialog();

    void setReference(const QString& reference);
    void setValue(const QString& value);
    void setShape(const QString& shape);
    void setOrdercode(const QString& ordercode);
    void setPrice(const double price);

    QString getReference() const;
    QString getValue() const;
    QString getShape() const;
    QString getOrdercode() const;
    double getPrice() const;

private:
    Ui::EditComponentDialog *ui;
};

#endif // EDITCOMPONENTDIALOG_H

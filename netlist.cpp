#include <QDebug>
#include "netlist.h"

Netlist::Netlist()
{

}

Netlist::~Netlist()
{
    foreach (Net* net, netlist) {
        delete (net);
    }
}

void Netlist::addDrawable(Drawable *item)
{
    if ((item->getClassName() == "wire") || (item->getClassName() == "junction")) {
        Net* net = new Net(static_cast<Connection*>(item));
        netlist.append(net);
    } else if (item->getClassName() == "component") {
        components.append(static_cast<Component*>(item));
    }
}

// Find nets that are connected to each other and combine them until all nets are separate.
void Netlist::joinMatchingNets()
{
    bool merged = true;
    while (merged) {
        merged = false;
        QList<Net*>::iterator outer = netlist.begin();
        while (outer != netlist.end()) {
            for (QList<Net*>::iterator inner = netlist.begin(); inner != netlist.end(); inner++) {
                if (inner != outer) {
                    bool match = false;
                    foreach(Coordinates c, (*inner)->getCoordinates()) {
                        if ((*outer)->isConnectedTo(c)) {
                            match = true;
                            break;
                        }
                    }
                    if (match) {
                        (*outer)->merge(*inner);
                        (*inner)->clear();
                        merged = true;
                    }
                }
            }
            outer++;
        }
    }

    // Now remove empty nets.
    QList<Net*>::iterator it = netlist.begin();
    while (it != netlist.end()) {
        if ((*it)->isEmpty()) {
            delete (*it);
            it = netlist.erase(it);
        } else {
            it++;
        }
    }
}

void Netlist::saveNetFile(QTextStream& stream)
{
    // First make a map with endpoints.
    QMap<uint, QString> endpoints;
    foreach (Component* component, components) {
        foreach (Pin* pin, component->getPins()) {
            Coordinates base;
            Coordinates coordinate;
            Pin::Side side;
            component->calculatePinPoint(*pin, &base, &coordinate, &side);
            QString s = component->getReference() + "(" + QString::number(pin->getNumber()) + ")";
            endpoints.insert(qHash(coordinate), s);
        }
    }

    int i = 1;
    foreach (Net* net, netlist) {
        int cnt = 0;
        QString s = "/N" + QString::number(i).rightJustified(5, '0');
        foreach (Coordinates c, net->getCoordinates()) {
            if (endpoints.contains(qHash(c))) {
                s += " " + endpoints.value(qHash(c));
                cnt++;
            }
        }
        if (cnt) {
            stream << s + ";\n";
            i++;
        }
    }
}

void Netlist::saveCmpFile(QTextStream &stream)
{
    foreach (Component* component, components) {
        stream << component->getValue().leftJustified(19, ' ') + " ";
        stream << component->getReference().leftJustified(19, ' ') + " ";
        stream << component->getShape().leftJustified(19, ' ') + " ";
        stream << "000     000     0\n";
    }
}

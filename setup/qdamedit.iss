; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "QScheme"
#define MyAppVersion "0.1.0"
#define MyAppPublisher "TwisQ"
#define MyAppURL "http://www.twisq.nl"
#define MyAppExeName "QScheme.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{3D9DF652-F465-40A8-BDE1-F5289C34A284}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DisableProgramGroupPage=yes
OutputBaseFilename=QSchemeInstall_v0_1_0
Compression=lzma
SolidCompression=yes
;ArchitecturesInstallIn64BitMode=x64

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "dutch"; MessagesFile: "compiler:Languages\Dutch.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "D:\workspace\qscheme\setup\deploy\QScheme.exe"; DestDir: "{app}";
Source: "D:\workspace\qscheme\setup\deploy\*.dll"; DestDir: "{app}";
Source: "D:\workspace\qscheme\lib\*"; DestDir: "{app}\lib";
Source: "D:\workspace\qscheme\setup\deploy\iconengines\*"; DestDir: "{app}\iconengines";
Source: "D:\workspace\qscheme\setup\deploy\imageformats\*"; DestDir: "{app}\imageformats";
Source: "D:\workspace\qscheme\setup\deploy\platforms\*"; DestDir: "{app}\platforms";
Source: "D:\workspace\qscheme\setup\deploy\translations\*"; DestDir: "{app}\translations";
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{commonprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

#include "library.h"

Library* Library::instance = nullptr;

Library::Library()
{
}

Library::~Library()
{
    clear();
    instance = nullptr;
}

Library* Library::getInstance()
{
    if (!instance) instance = new Library();
    return instance;
}

void Library::addSymbol(Symbol *symbol)
{
    symbols.insert(symbol->getName(), symbol);
}

Symbol* Library::getSymbol(const QString &key) const
{
    return symbols.value(key, nullptr);
}

QList<Symbol*> Library::getAllSymbols() const
{
    return symbols.values();
}

void Library::clear()
{
    foreach (Symbol* s, getAllSymbols()) {
        delete s;
    }
    symbols.clear();
}

#include <QInputDialog>
#include "qprojectview.h"
#include "text.h"

Text::Text(Sheet* sheet)
    : Drawable(sheet)
{
    value = "";
    height = 1.0;
    rightAllign = false;
}

void Text::toJson(QJsonObject &obj)
{
    Drawable::toJson(obj);
    obj.insert("value", value);
    obj.insert("height", height);
    obj.insert("rightAllign", rightAllign);
}

bool Text::fromJson(const QJsonValue &json)
{
    QJsonObject obj = json.toObject();
    value = obj.value("value").toString();
    height = obj.value("height").toDouble();
    rightAllign = obj.value("rightAllign").toBool();
    return Drawable::fromJson(json);
}

void Text::draw(QProjectView *view)
{
    if (lastPosition.hasValue() && lastPosition.getOn_sheet(sheet)) {
        view->drawText(lastPosition, value, angle, rightAllign, height, TEXT_COLOR);
    }
}

void Text::put()
{
    sheet->addDrawable(this);
    sheet->setActiveDrawable(new Text(*this));
}

void Text::change()
{
    rightAllign = !rightAllign;
}

void Text::edit()
{
    bool ok;
    QString text = QInputDialog::getText(nullptr, "Edit text", "New text:", QLineEdit::Normal, value, &ok);
    if (ok && !text.isEmpty()) value = text;
}

QString Text::getValue() const
{
    return value;
}

void Text::setValue(const QString &value)
{
    this->value = value;
}

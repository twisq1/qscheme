#ifndef NETLIST_H
#define NETLIST_H

#include <QList>
#include <QTextStream>
#include "net.h"
#include "component.h"
#include "drawable.h"

class Netlist
{
public:
    Netlist();
    virtual ~Netlist();

    void addDrawable(Drawable* item);
    void joinMatchingNets();
    void saveNetFile(QTextStream& stream);
    void saveCmpFile(QTextStream& stream);

private:
    QList<Component*> components;
    QList<Net*> netlist;
};

#endif // NETLIST_H

#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <QString>
#include <QJsonObject>
#include <QJsonValue>
#include "coordinates.h"

class Sheet;
class QProjectView;

class Drawable
{
public:
    Drawable(Sheet* sheet);
    virtual ~Drawable();

    static Drawable* create(QString type, Sheet* sheet);

    virtual void draw(QProjectView* view) = 0;

    virtual const QString getClassName() = 0;
    virtual void toJson(QJsonObject& obj);
    virtual bool fromJson(const QJsonValue& json);
    virtual bool isPresentAt(const Coordinates& p);

    virtual void cursorMoved(const Coordinates& position);
    virtual void leftClick(const Coordinates& position);
    virtual void rightClick(const Coordinates& position);
    virtual void again();
    virtual void put();
    virtual void end();
    virtual void escape();
    virtual void change();
    virtual void rotate();
    virtual void mirror();
    virtual void edit();

    Coordinates getLastPosition() const;
    void setLastPosition(const Coordinates &value);
    void setDirty(bool value = true);

protected:
    Coordinates lastPosition;
    Sheet* sheet;
    int angle;
    bool mirrored;
};

#endif // DRAWABLE_H

#include "mainwindow.h"
#include <QApplication>
#include "main.h"

int main(int argc, char *argv[])
{
    // Gloabl settings
    QCoreApplication::setOrganizationName(MANUFACTURER);
    QCoreApplication::setApplicationVersion(VERSION);
    QCoreApplication::setOrganizationDomain(URL);
    QCoreApplication::setApplicationName(APPLICATION);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    a.installEventFilter(&w);

    return a.exec();
}

#include <math.h>
#include <QPainter>
#include <QMouseEvent>
#include <QStyleOption>
#include <QStyle>
#include <QDebug>

#include "qprojectview.h"

QProjectView::QProjectView(QWidget *parent) : QWidget(parent)
{
    painter = nullptr;
    worksheet = nullptr;
    zoom = 1.0;
    show_grid = true;
    grid_valid = false;
    offset = QPoint(BORDER_SIZE, BORDER_SIZE);
    setMouseTracking(true);
}

QProjectView::~QProjectView()
{
}

void QProjectView::setWorksheet(Sheet *worksheet)
{
    this->worksheet = worksheet;
}

QSize QProjectView::getSize()
{
    return sizeHint();
}

void QProjectView::paintEvent(QPaintEvent *evt)
{
    Q_UNUSED(evt);

    Q_ASSERT_X(worksheet, "paintEvent", "There is no worksheet to paint.");

    // Get painter
    QPainter painter(this);

    // Draw sheet.
    this->painter = &painter;
    if (!grid_valid) calculateGrid();
    worksheet->draw(this);

    // Draw grid.
    if (show_grid) {
        QPen oldpen = painter.pen();
        painter.setPen(GRID_COLOR);
        painter.drawPoints(grid);
        painter.setPen(oldpen);
    }

    this->painter = nullptr;
}

void QProjectView::mousePressEvent(QMouseEvent *evt)
{
    Q_ASSERT_X(worksheet, "mouseMoveEvent", "No sheet.");

    Coordinates coordinates(screenToCoordinates(evt->pos()).snapToGrid());
    if (coordinates.getOn_sheet(worksheet)) {
        if(evt->button() == Qt::LeftButton){
            worksheet->leftClick(coordinates);
        } else if (evt->button() == Qt::RightButton) {
            worksheet->rightClick(coordinates);
        }
        update();
    }
}

void QProjectView::mouseMoveEvent(QMouseEvent *evt)
{
    Q_ASSERT_X(worksheet, "mouseMoveEvent", "No sheet.");

    Coordinates coordinates(screenToCoordinates(evt->pos()).snapToGrid());
    if (coordinates.getOn_sheet(worksheet) &&
            (cursorPosition != coordinates.toPoint())) {
        cursorPosition = coordinates.toPoint();
        worksheet->cursorMoved(coordinates);
        emit cursorMoved(cursorPosition);
        update();
    }
}

QSize QProjectView::sizeHint() const
{
    return QSize(worksheet->getSize().x() * GRID_SIZE * zoom + 2 * offset.x(),
                 worksheet->getSize().y() * GRID_SIZE * zoom + 2 * offset.y());
}

QPoint QProjectView::coordinatesToScreen(Coordinates coordinates)
{
    QPoint retval(round(coordinates.x() * GRID_SIZE * zoom) + offset.x(),
                  round(coordinates.y() * GRID_SIZE * zoom) + offset.y());
    return retval;
}

Coordinates QProjectView::screenToCoordinates(QPoint screen)
{
    double x = double(screen.x() - offset.x()) / (GRID_SIZE * zoom);
    double y = double(screen.y() - offset.y()) / (GRID_SIZE * zoom);
    Coordinates retval(x, y);
    return retval;
}

void QProjectView::rotatePoint(QPoint &p, int angle)
{
    angle = angle % 360;
    if (angle < 0) angle += 360;

    int x = p.x();
    int y = p.y();
    switch (angle) {
    case 0:
        break;
    case 90:
        x = -p.y();
        y = p.x();
        break;
    case 180:
        x = -p.x();
        y = -p.y();
        break;
    case 270:
        x = p.y();
        y = -p.x();
        break;
    default:
        qWarning() << "Illegal angle in rotatePoint.";
        break;
    }
    p.setX(x);
    p.setY(y);
}

void QProjectView::rotateRectangle(QRect &r, int angle)
{
    QPoint p1 = r.topLeft();
    QPoint p2 = r.bottomRight();
    rotatePoint(p1, angle);
    rotatePoint(p2, angle);
    QRect rnew;
    rnew.setTopLeft(p1);
    rnew.setBottomRight(p2);
    r = rnew.normalized();
}

Coordinates QProjectView::getCursorCoordinates()
{
    return screenToCoordinates(mapFromGlobal(QCursor::pos())).snapToGrid();
}

void QProjectView::setCursorCoordinates(Coordinates newpos)
{
    QCursor::setPos(mapToGlobal(coordinatesToScreen(newpos)));
}

void QProjectView::calculateGrid()
{
    Q_ASSERT_X(worksheet, "calculateGrid", "No worksheet");
    grid.clear();
    for (int x = 1; x < worksheet->getSize().x(); x++) {
        for (int y = 1; y < worksheet->getSize().y(); y++) {
            grid << coordinatesToScreen(Coordinates(x, y));
        }
    }
    grid_valid = true;
}

void QProjectView::drawPoint(Coordinates point, int width, int color)
{
    Q_ASSERT_X(painter, "drawPoint", "Attempting to draw without painter.");

    QPen pen;
    pen.setColor(color);
    pen.setWidth(width);
    painter->setPen(pen);
    painter->drawPoint(coordinatesToScreen(point));
}

void QProjectView::drawLine(Coordinates start, Coordinates end, int width, int color, int style)
{
    Q_ASSERT_X(painter, "drawLine", "Attempting to draw without painter.");

    QPen pen;
    pen.setColor(color);
    pen.setWidth(width);
    pen.setStyle(Qt::PenStyle(style));
    painter->setPen(pen);
    painter->drawLine(coordinatesToScreen(start), coordinatesToScreen(end));
}

void QProjectView::drawRectangle(Coordinates start, Coordinates end, int width, int line_color, int fill_color, int line_style)
{
    Q_ASSERT_X(painter, "drawRectangle", "Attempting to draw without painter.");

    QPen pen;
    pen.setColor(line_color);
    pen.setWidth(width);
    pen.setStyle(Qt::PenStyle(line_style));
    painter->setPen(pen);

    QBrush brush;
    if (fill_color >= 0) {
        brush.setColor(fill_color);
        brush.setStyle(Qt::SolidPattern);
    } else {
        brush.setStyle(Qt::NoBrush);
    }
    painter->setBrush(brush);

    painter->drawRect(QRect(coordinatesToScreen(start), coordinatesToScreen(end)));
}

void QProjectView::drawEllipse(Coordinates start, Coordinates end, int width, int line_color, int fill_color, int line_style)
{
    Q_ASSERT_X(painter, "drawEllipse", "Attempting to draw without painter.");

    QPen pen;
    pen.setColor(line_color);
    pen.setWidth(width);
    pen.setStyle(Qt::PenStyle(line_style));
    painter->setPen(pen);

    QBrush brush;
    if (fill_color >= 0) {
        brush.setColor(fill_color);
        brush.setStyle(Qt::SolidPattern);
        painter->setBrush(brush);
    } else {
        brush.setStyle(Qt::NoBrush);
    }

    painter->drawEllipse(QRect(coordinatesToScreen(start), coordinatesToScreen(end)));
}

void QProjectView::drawText(Coordinates start, const QString &s, int angle, bool rightAllign, double height, int color)
{
    // Normalize angle
    angle %= 360;
    if (angle < 0) angle += 360;

    Q_ASSERT_X(!(angle % 90), "drawText", "Illegal angle, only 0, 90, 180 and 270 are allowed.");
    Q_ASSERT_X(painter, "drawText", "Attempting to draw without painter.");

    // Calculate text height.
    int pixheight = height * zoom * GRID_SIZE;

    // Get font
    QFont f("Helvetica");
    f.setPixelSize(pixheight);
    painter->setFont(f);

    // Get pen
    QPen pen;
    pen.setColor(color);
    pen.setWidth(1);
    pen.setStyle(Qt::SolidLine);
    painter->setPen(pen);

    // Calculate size of string.
    QFontMetrics metrics(f);
    int pixwidth = metrics.width(s);

    // Calculate rectangle in which to put the text.
    QPoint p = coordinatesToScreen(start);
    QRect rect;
    switch (angle) {
    case 0:
        if (rightAllign) {
            rect.setTop(p.y());
            rect.setLeft(p.x() - pixwidth);
            rect.setHeight(pixheight);
            rect.setWidth(pixwidth);
        } else {
            rect.setTop(p.y());
            rect.setLeft(p.x());
            rect.setHeight(pixheight);
            rect.setWidth(pixwidth);
        }
        break;
    case 90:
        if (rightAllign) {
            rect.setTop(p.y());
            rect.setLeft(p.x());
            rect.setHeight(pixwidth);
            rect.setWidth(pixheight);
        } else {
            rect.setTop(p.y() - pixwidth);
            rect.setLeft(p.x());
            rect.setHeight(pixwidth);
            rect.setWidth(pixheight);
        }
        break;
    case 180:
        if (rightAllign) {
            rect.setTop(p.y() - pixheight);
            rect.setLeft(p.x());
            rect.setHeight(pixheight);
            rect.setWidth(pixwidth);
        } else {
            rect.setTop(p.y() - pixheight);
            rect.setLeft(p.x() - pixwidth);
            rect.setHeight(pixheight);
            rect.setWidth(pixwidth);
        }
        break;
    case 270:
        if (rightAllign) {
            rect.setTop(p.y() - pixwidth);
            rect.setLeft(p.x() - pixheight);
            rect.setHeight(pixwidth);
            rect.setWidth(pixheight);
        } else {
            rect.setTop(p.y());
            rect.setLeft(p.x() - pixheight);
            rect.setHeight(pixwidth);
            rect.setWidth(pixheight);
        }
        break;
    }

    // Draw text
    painter->save();
    painter->rotate(-angle);
    rotateRectangle(rect, angle);
    //painter->drawRect(rect);
    painter->drawText(rect, Qt::AlignVCenter, s);
    painter->restore();
}

void QProjectView::drawImage(Coordinates position, const QPixmap &pixmap)
{
    Q_ASSERT_X(painter, "drawImage", "Attempting to draw without painter.");

    int width = pixmap.width() * zoom;
    int height = pixmap.height() * zoom;
    painter->drawPixmap(coordinatesToScreen(position), pixmap.scaled(width, height));
}

bool QProjectView::getShow_grid() const
{
    return show_grid;
}

void QProjectView::setShow_grid(bool value)
{
    show_grid = value;
    update();
}

double QProjectView::getZoom() const
{
    return zoom;
}

void QProjectView::setZoom(double value)
{
    zoom = value;
    grid_valid = false;
    update();
}

void QProjectView::moveUp()
{
    Coordinates pos = getCursorCoordinates();
    if (pos.getOn_sheet(worksheet) && (pos.y() >= 1.0)) {
        pos.setY(pos.y() - 1);
        setCursorCoordinates(pos);
    }
}

void QProjectView::moveDown()
{
    Q_ASSERT_X(worksheet, "moveDown", "No worksheet");

    Coordinates pos = getCursorCoordinates();
    if (pos.getOn_sheet(worksheet) && (pos.y() <= (worksheet->getSize().y() - 1.0))) {
        pos.setY(pos.y() + 1);
        setCursorCoordinates(pos);
    }
}

void QProjectView::moveLeft()
{
    Coordinates pos = getCursorCoordinates();
    if (pos.getOn_sheet(worksheet) && (pos.x() >= 1.0)) {
        pos.setX(pos.x() - 1);
        setCursorCoordinates(pos);
    }
}

void QProjectView::moveRight()
{
    Q_ASSERT_X(worksheet, "moveRight", "No worksheet");

    Coordinates pos = getCursorCoordinates();
    if (pos.getOn_sheet(worksheet) && (pos.x() <= (worksheet->getSize().x() - 1.0))) {
        pos.setX(pos.x() + 1);
        setCursorCoordinates(pos);
    }
}

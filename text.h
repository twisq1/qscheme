#ifndef TEXT_H
#define TEXT_H

#include <QString>
#include "drawable.h"

class QProjectView;

class Text : public Drawable
{
    static const int TEXT_COLOR = 0;

public:
    Text(Sheet* sheet);

    // Overrides
    virtual const QString getClassName() { return "text"; }
    virtual void toJson(QJsonObject &obj);
    virtual bool fromJson(const QJsonValue &json);

    virtual void draw(QProjectView* view);
    virtual void put();
    virtual void change();
    virtual void edit();

    QString getValue() const;
    void setValue(const QString &value);

private:
    QString value;
    bool rightAllign;
    double height;
};

#endif // TEXT_H

#include "editcomponentdialog.h"
#include "ui_editcomponentdialog.h"

EditComponentDialog::EditComponentDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditComponentDialog)
{
    ui->setupUi(this);
}

EditComponentDialog::~EditComponentDialog()
{
    delete ui;
}

void EditComponentDialog::setReference(const QString &reference)
{
    ui->le_reference->setText(reference);
}

void EditComponentDialog::setValue(const QString &value)
{
    ui->le_value->setText(value);
}

void EditComponentDialog::setShape(const QString &shape)
{
    ui->le_shape->setText(shape);
}

void EditComponentDialog::setOrdercode(const QString &ordercode)
{
    ui->le_ordercode->setText(ordercode);
}

void EditComponentDialog::setPrice(const double price)
{
    ui->le_price->setText(QString::number(price));
}

QString EditComponentDialog::getReference() const
{
    return ui->le_reference->text();
}

QString EditComponentDialog::getValue() const
{
    return ui->le_value->text();
}

QString EditComponentDialog::getShape() const
{
    return ui->le_shape->text();
}

QString EditComponentDialog::getOrdercode() const
{
    return ui->le_ordercode->text();
}

double EditComponentDialog::getPrice() const
{
    return ui->le_price->text().toDouble();
}

#ifndef QPROJECTVIEW_H
#define QPROJECTVIEW_H

#include <QWidget>
#include <QPolygon>
#include <QPoint>
#include <QRect>
#include "sheet.h"
#include "wire.h"
#include "image.h"
#include "coordinates.h"

class QProjectView : public QWidget
{
    Q_OBJECT

    static const int GRID_COLOR = 0xa0a0a0;
    static const int BORDER_SIZE = 4;

public:
    static const int GRID_SIZE = 10;

    explicit QProjectView(QWidget *parent = 0);
    virtual ~QProjectView();

    // Drawing functions
    void drawPoint(Coordinates point, int width = 1, int color = 0);
    void drawLine(Coordinates start, Coordinates end, int width = 1, int color = 0, int style = Qt::SolidLine);
    void drawRectangle(Coordinates start, Coordinates end, int width = 1, int line_color = 0, int fill_color = -1, int line_style = Qt::SolidLine);
    void drawEllipse(Coordinates start, Coordinates end, int width = 1, int line_color = 0, int fill_color = -1, int line_style = Qt::SolidLine);
    void drawText(Coordinates start, const QString& s, int angle = 0, bool rightAllign = false, double height = 1.0, int color = 0);
    void drawImage(Coordinates position, const QPixmap& pixmap);

    // Getters and setters
    void setWorksheet(Sheet* worksheet);
    bool getShow_grid() const;
    void setShow_grid(bool value);
    double getZoom() const;
    void setZoom(double value);
    QSize getSize();
    Coordinates getCursorCoordinates();
    void setCursorCoordinates(Coordinates newpos);

    // Actions
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();

signals:
    void cursorMoved(QPoint pos);

public slots:

private:
    // Global properties
    bool show_grid;

    // Zoom and offset
    double zoom;
    QPoint offset;
    bool grid_valid;          // If false, the grid should be recalculated.

    // The main worksheet
    Sheet* worksheet;

    // During the paint event, this is the painter to use.
    QPainter* painter;

    // This is the collection of grid points.
    QPolygon grid;

    // Last known cursor position.
    QPoint cursorPosition;

    // Overrides
    void paintEvent(QPaintEvent *evt);
    void mousePressEvent(QMouseEvent *evt);
    void mouseMoveEvent(QMouseEvent *evt);

    virtual QSize sizeHint() const;

    // Translation between screen and sheet coordinates
    QPoint coordinatesToScreen(Coordinates coordinates);
    Coordinates screenToCoordinates(QPoint screen);

    // Helper functions for rotations
    void rotatePoint(QPoint& p, int angle);
    void rotateRectangle(QRect& r, int angle);

    // Miscelanious private functions
    void calculateGrid();
};

#endif // QPROJECTVIEW_H

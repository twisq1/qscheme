#-------------------------------------------------
#
# Project created by QtCreator 2017-10-01T19:46:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QScheme
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    qprojectview.cpp \
    sheet.cpp \
    wire.cpp \
    coordinates.cpp \
    drawable.cpp \
    image.cpp \
    component.cpp \
    junction.cpp \
    sender.cpp \
    text.cpp \
    pin.cpp \
    symbol.cpp \
    library.cpp \
    editcomponentdialog.cpp \
    net.cpp \
    connection.cpp \
    netlist.cpp

HEADERS  += mainwindow.h \
    qprojectview.h \
    sheet.h \
    wire.h \
    coordinates.h \
    drawable.h \
    image.h \
    component.h \
    junction.h \
    sender.h \
    text.h \
    pin.h \
    main.h \
    symbol.h \
    library.h \
    editcomponentdialog.h \
    connection.h \
    net.h \
    netlist.h

FORMS    += \
    editcomponentdialog.ui

DISTFILES += \
    .gitignore \
    ../qscheme/lib/diode.cmp \
    ../qscheme/lib/npn.cmp \
    ../qscheme/lib/regulator.cmp \
    lib/DEVICE.TXT \
    lib/74165.sbl \
    lib/75lbc184.sbl \
    lib/attiny2313.sbl \
    lib/battery.sbl \
    lib/capacitor-pol.sbl \
    lib/capacitor.sbl \
    lib/crystal.sbl \
    lib/diode.sbl \
    lib/dipswitch8.sbl \
    lib/header2.sbl \
    lib/header2x5.sbl \
    lib/header3.sbl \
    lib/header4.sbl \
    lib/npn.sbl \
    lib/pnp.sbl \
    lib/regulator.sbl \
    lib/resistor.sbl \
    lib/rnet.sbl \
    lib/uln2803.sbl \
    lib/header2x3.sbl \
    lib/led.sbl

#ifndef COORDINATES_H
#define COORDINATES_H

#include <QPointF>

class Sheet;

class Coordinates : public QPointF
{
public:
    Coordinates();
    Coordinates(int x, int y);
    Coordinates(double x, double y);
    Coordinates(QPointF p);
    Coordinates& operator=(QPointF p);
    uint qHash() const;

    Coordinates snapToGrid();

    bool hasValue() const;

    // Overrides
    void setX(qreal x);
    void setY(qreal y);

    bool getOn_sheet(Sheet* sheet) const;

private:
    bool has_value;
};

// Because we want to make a set of coordinates, we need a hash function for Coordinates.
uint qHash(const Coordinates &coordinates);

#endif // COORDINATES_H

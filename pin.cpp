#include "pin.h"

Pin::Pin(QString position, int number, QString name, Pin::LineType lt, Pin::ElecType et)
{
    this->position = position;
    this->number = number;
    this->name = name;
    this->linetype = lt;
    this->electype = et;
}

QString Pin::getPosition() const
{
    return position;
}

int Pin::getNumber() const
{
    return number;
}

QString Pin::getName() const
{
    return name;
}

Pin::LineType Pin::getLinetype() const
{
    return linetype;
}

Pin::ElecType Pin::getElectype() const
{
    return electype;
}

#ifndef SENDER_H
#define SENDER_H

#include <QObject>

class Sender : public QObject
{
    Q_OBJECT
public:
    explicit Sender(QObject *parent = nullptr);

    void send(QString s);

signals:
    void setStatusText(QString s);
    void enableKeyboard(bool enable);


public slots:
};

#endif // SENDER_H

#include <QPainter>
#include <QFileInfo>
#include "qprojectview.h"
#include "library.h"
#include "symbol.h"
#include "image.h"

Image::Image(Sheet* sheet)
    : Drawable(sheet)
{
    symbol = nullptr;
}

Image::~Image()
{
}

void Image::toJson(QJsonObject &obj)
{
    Q_ASSERT_X(symbol, "toJson", "Symbol not loaded.");
    Drawable::toJson(obj);
    obj.insert("symbol", symbol->getName());
}

bool Image::fromJson(const QJsonValue &json)
{
    if (Drawable::fromJson(json)) {
        QJsonObject obj = json.toObject();
        QString symbolname = obj.value("symbol").toString();
        symbol = Library::getInstance()->getSymbol(symbolname);
        return true;
    }
    return false;
}

void Image::draw(QProjectView *view)
{
    Q_ASSERT_X(symbol, "draw", "Symbol not loaded.");
    if (lastPosition.hasValue() && lastPosition.getOn_sheet(sheet)) {
        view->drawImage(lastPosition, *(symbol->getPixmap()));
    }
}

void Image::put()
{
    sheet->addDrawable(this);
    sheet->setActiveDrawable(new Image(*this));
}

void Image::load(const QString filename)
{
    QFileInfo fi(filename);
    QString symbolname = fi.fileName();
    symbol = Library::getInstance()->getSymbol(symbolname);
    if (!symbol) {
        symbol = new Symbol();
        symbol->setName(symbolname);
        QPixmap* pm = new QPixmap();
        pm->load(filename);
        symbol->setPixmap(pm);
        Library::getInstance()->addSymbol(symbol);
    }
}

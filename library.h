#ifndef LIBRARY_H
#define LIBRARY_H

#include <QString>
#include <QMap>
#include <QList>
#include "symbol.h"

class Library
{
public:
    virtual ~Library();

    static Library* getInstance();

    void addSymbol(Symbol* symbol);
    Symbol* getSymbol(const QString& key) const;
    QList<Symbol*> getAllSymbols() const;
    void clear();

private:
    Library();

    static Library* instance;

    QMap<QString, Symbol*> symbols;
};

#endif // LIBRARY_H

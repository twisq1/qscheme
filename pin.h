#ifndef PIN_H
#define PIN_H

#include <QString>

class Pin
{
public:
    enum LineType {
        NONE,
        SHORT
    };

    enum ElecType {
        PASSIVE,
        INPUT,
        OUTPUT,
        IO,
        POWER
    };

    enum Side {
        TOP = 0,
        RIGHT = 1,
        BOTTOM = 2,
        LEFT = 3
    };

    Pin(QString position, int number, QString name, LineType lt = SHORT, ElecType el = PASSIVE);

    QString getPosition() const;
    int getNumber() const;
    QString getName() const;
    LineType getLinetype() const;
    ElecType getElectype() const;

private:
    QString position;
    int number;
    QString name;
    LineType linetype;
    ElecType electype;
};

#endif // PIN_H

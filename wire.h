#ifndef WIRE_H
#define WIRE_H

#include <QVector>
#include "coordinates.h"
#include "connection.h"

class QProjectView;

class Wire : public Connection
{
    static const int WIRE_SIZE = 1;
    static const int WIRE_COLOR = 0x000000;
    static const int WIRE_INDENT_COLOR = 0xff0000;
    static const int WIRE_STYLE = Qt::SolidLine;
    static const int BUS_SIZE = 3;
    static const int BUS_COLOR = 0x0000ff;
    static const int BUS_INDENT_COLOR = 0xff0000;
    static const int BUS_STYLE = Qt::SolidLine;
    static const int DASHED_SIZE = 1;
    static const int DASHED_COLOR = 0x808000;
    static const int DASHED_INDENT_COLOR = 0xff0000;
    static const int DASHED_STYLE = Qt::DashLine;

public:
    enum LineType {
        NONE,
        WIRE,
        BUS,
        DASHED
    };

    Wire(Sheet* sheet, LineType type = WIRE);
    virtual ~Wire();

    virtual const QString getClassName() { return "wire"; }
    virtual void toJson(QJsonObject &obj);
    virtual bool fromJson(const QJsonValue &json);

    // Overrides
    virtual void draw(QProjectView* view);
    virtual void again();
    virtual void put();
    virtual void end();
    virtual void change();
    virtual void escape();
    virtual void leftClick(const Coordinates &position);
    virtual void rightClick(const Coordinates &position);
    virtual void cursorMoved(const Coordinates &position);
    virtual bool isConnectedTo(Coordinates p);
    virtual QSet<Coordinates> getCoordinates();

    void setIndent(bool value);

private:
    QVector<Coordinates> points;
    bool indent;

    enum {
        POINT,
        HORIZONTAL_VERTICAL,
        VERTICAL_HORIZONTAL,
        DIRECT,
        CLOSED
    } mode_last_segment;

    enum LineType line_type;

    void close();
};

#endif // WIRE_H

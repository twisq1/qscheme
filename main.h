#ifndef MAIN_H
#define MAIN_H

#include <QString>

const QString MANUFACTURER = "TwisQ";
const QString URL = "https://www.twisq.nl/qscheme";
const QString APPLICATION = "QScheme";
const QString VERSION = "0.0.1";

#endif // MAIN_H

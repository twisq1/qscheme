#ifndef NET_H
#define NET_H

#include <QList>
#include "connection.h"
#include "coordinates.h"

class Net : public Connection
{
public:
    Net(Connection* connection);
    virtual ~Net();

    void merge(Net* net);
    void clear();
    bool isEmpty();

    // Overrides
    virtual void draw(QProjectView* view);
    virtual const QString getClassName();
    virtual bool isConnectedTo(Coordinates p);
    virtual QSet<Coordinates> getCoordinates();

private:
    QList<Connection*> connections;
};

#endif // NET_H

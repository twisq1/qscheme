#include <QJsonArray>
#include <QJsonObject>
#include <QByteArray>
#include <QBuffer>
#include "symbol.h"


Symbol::Symbol()
{
    size.setX(0);
    size.setY(0);
    name = "";
    pins.clear();
    pixmap = nullptr;
}

Symbol::~Symbol()
{
    foreach(Pin* pin, pins) {
        delete pin;
    }
    pins.clear();

    delete pixmap;
}

void Symbol::toJson(QJsonObject &obj)
{
    obj.insert("symbol", name);
    obj.insert("reference", reference);
    obj.insert("shape", shape);
    obj.insert("value", value);
    obj.insert("ordercode", ordercode);
    obj.insert("price", price);
    obj.insert("sizex", size.x());
    obj.insert("sizey", size.y());

    if (pixmap) {
        QByteArray bytes;
        QBuffer buffer(&bytes);
        buffer.open(QIODevice::WriteOnly);
        pixmap->save(&buffer, "PNG"); // writes pixmap into bytes in PNG format
        obj.insert("png", QString::fromLatin1(buffer.data().toBase64()));
    }

    QJsonArray array;
    foreach (Pin* pin, pins) {
        QJsonObject pinobj;
        pinobj.insert("name", pin->getName());
        pinobj.insert("number", pin->getNumber());
        pinobj.insert("position", pin->getPosition());
        pinobj.insert("linetype", pin->getLinetype());
        pinobj.insert("electype", pin->getElectype());
        array.append(pinobj);
    }
    obj.insert("pins", array);
}

bool Symbol::fromJson(const QJsonValue &json)
{
    if (!json.isObject()) return false;
    QJsonObject obj = json.toObject();
    name = obj.value("symbol").toString();
    reference = obj.value("reference").toString();
    shape = obj.value("shape").toString();
    value = obj.value("value").toString();
    ordercode = obj.value("ordercode").toString();
    price = obj.value("price").toDouble();
    size.setX(obj.value("sizex").toInt());
    size.setY(obj.value("sizey").toInt());

    // Load PNG
    if (obj.contains("png")) {
        QString base64 = obj.value("png").toString();
        QByteArray ba = QByteArray::fromBase64(base64.toLatin1());
        pixmap = new QPixmap();
        pixmap->loadFromData(ba, "PNG");
    }

    // Load pins
    foreach (QJsonValue pinjson, obj.value("pins").toArray()) {
        QJsonObject pinobj = pinjson.toObject();
        QString name = pinobj.value("name").toString();
        int number = pinobj.value("number").toInt();
        QString position = pinobj.value("position").toString();
        Pin::LineType linetype = static_cast<Pin::LineType>(pinobj.value("linetype").toInt());
        Pin::ElecType electype = static_cast<Pin::ElecType>(pinobj.value("electype").toInt());
        Pin* pin = new Pin(position, number, name, linetype, electype);
        addPin(pin);
    }

    return true;
}

QPoint Symbol::getSize() const
{
    return size;
}

void Symbol::setSize(const QPoint &value)
{
    size = value;
}

QString Symbol::getName() const
{
    return name;
}

void Symbol::setName(const QString &value)
{
    name = value;
}

QList<Pin *> Symbol::getPins() const
{
    return pins;
}

void Symbol::addPin(Pin *p)
{
    pins.append(p);
}

QPixmap *Symbol::getPixmap() const
{
    return pixmap;
}

void Symbol::setPixmap(QPixmap *value)
{
    pixmap = value;
}

QString Symbol::getReference() const
{
    return reference;
}

void Symbol::setReference(const QString &value)
{
    reference = value;
}

QString Symbol::getValue() const
{
    return value;
}

void Symbol::setValue(const QString &value)
{
    this->value = value;
}

QString Symbol::getShape() const
{
    return shape;
}

void Symbol::setShape(const QString &value)
{
    shape = value;
}

QString Symbol::getOrdercode() const
{
    return ordercode;
}

void Symbol::setOrdercode(const QString &value)
{
    ordercode = value;
}

double Symbol::getPrice() const
{
    return price;
}

void Symbol::setPrice(double value)
{
    price = value;
}

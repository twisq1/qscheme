#include <QCursor>
#include <QScrollBar>
#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QKeyEvent>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTextStream>
#include <QMessageBox>
#include <QSettings>
#include "qprojectview.h"
#include "junction.h"
#include "component.h"
#include "text.h"
#include "main.h"
#include "library.h"
#include "netlist.h"
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    scrollArea(new QScrollArea),
    view(new QProjectView)
{
    resize(1200, 960);
    view->setWorksheet(&worksheet);
    scrollArea->setWidget(view);
    setCentralWidget(scrollArea);
    scrollArea->setVisible(true);

    actionWire = new QAction(this);
    actionWire->setObjectName(QStringLiteral("actionWire"));
    actionWire->setCheckable(false);
    actionBus = new QAction(this);
    actionBus->setObjectName(QStringLiteral("actionBus"));
    actionBus->setCheckable(false);
    actionDashed = new QAction(this);
    actionDashed->setObjectName(QStringLiteral("actionDashed"));
    actionDashed->setCheckable(false);
    actionImage = new QAction(this);
    actionImage->setObjectName(QStringLiteral("actionImage"));
    actionImage->setCheckable(false);
    actionComponent = new QAction(this);
    actionComponent->setObjectName(QStringLiteral("actionComponent"));
    actionComponent->setCheckable(false);
    actionJunction = new QAction(this);
    actionJunction->setObjectName(QStringLiteral("actionJunction"));
    actionText = new QAction(this);
    actionText->setObjectName(QStringLiteral("actionText"));
    actionLabel = new QAction(this);
    actionLabel->setObjectName(QStringLiteral("actionLabel"));
    actionEntry = new QAction(this);
    actionEntry->setObjectName(QStringLiteral("actionKeyE"));
    actionSheet = new QAction(this);
    actionSheet->setObjectName(QStringLiteral("actionSheet"));
    actionModulePort = new QAction(this);
    actionModulePort->setObjectName(QStringLiteral("actionModulePort"));
    actionShow_grid = new QAction(this);
    actionShow_grid->setObjectName(QStringLiteral("actionShow_grid"));
    actionShow_grid->setCheckable(true);
    actionZoom_in = new QAction(this);
    actionZoom_in->setObjectName(QStringLiteral("actionZoom_in"));
    actionZoom_out = new QAction(this);
    actionZoom_out->setObjectName(QStringLiteral("actionZoom_out"));
    actionZoom_reset = new QAction(this);
    actionZoom_reset->setObjectName(QStringLiteral("actionZoom_reset"));
    actionExit = new QAction(this);
    actionExit->setObjectName(QStringLiteral("actionExit"));
    actionSave = new QAction(this);
    actionSave->setObjectName(QStringLiteral("actionSave"));
    actionLoad = new QAction(this);
    actionLoad->setObjectName(QStringLiteral("actionLoad"));
    actionNew = new QAction(this);
    actionNew->setObjectName(QStringLiteral("actionNewDoc"));
    actionNetlist = new QAction(this);
    actionNetlist->setObjectName("actionNetlist");
    actionDelete = new QAction(this);
    actionDelete->setObjectName("actionDelete");
    actionEdit = new QAction(this);
    actionEdit->setObjectName("actionEdit");
    menuBar = new QMenuBar(this);
    menuBar->setNativeMenuBar(false);
    menuBar->setObjectName(QStringLiteral("menuBar"));
    menuBar->setGeometry(QRect(0, 0, 1024, 22));
    menuFile = new QMenu(menuBar);
    menuFile->setObjectName(QStringLiteral("menuFile"));
    menuPlace = new QMenu(menuBar);
    menuPlace->setObjectName(QStringLiteral("menuPlace"));
    menuView = new QMenu(menuBar);
    menuView->setObjectName(QStringLiteral("menuView"));
    menuTools = new QMenu(menuBar);
    menuTools->setObjectName("Tools");
    menuEdit = new QMenu(menuBar);
    menuEdit->setObjectName("Edit");
    this->setMenuBar(menuBar);
    statusBar = new QStatusBar(this);
    statusBar->setObjectName(QStringLiteral("statusBar"));
    this->setStatusBar(statusBar);
    statusText = new QLabel(this);
    statusBar->addWidget(statusText);
    cursorPosition = new QLabel(this);
    cursorPosition->setAlignment(Qt::AlignRight);
    statusBar->addPermanentWidget(cursorPosition);

    menuBar->addAction(menuFile->menuAction());
    menuBar->addAction(menuEdit->menuAction());
    menuBar->addAction(menuPlace->menuAction());
    menuBar->addAction(menuView->menuAction());
    menuBar->addAction(menuTools->menuAction());
    menuFile->addAction(actionNew);
    menuFile->addAction(actionLoad);
    menuFile->addAction(actionSave);
    menuFile->addSeparator();
    menuFile->addAction(actionExit);
    menuPlace->addAction(actionWire);
    menuPlace->addAction(actionJunction);
    //menuPlace->addSeparator();
    //menuPlace->addAction(actionEntry);
    //menuPlace->addAction(actionLabel);
    //menuPlace->addSeparator();
    menuPlace->addAction(actionComponent);
    menuPlace->addSeparator();
    //menuPlace->addAction(actionSheet);
    //menuPlace->addAction(actionModulePort);
    //menuPlace->addSeparator();
    menuPlace->addAction(actionBus);
    menuPlace->addAction(actionImage);
    menuPlace->addAction(actionText);
    menuPlace->addAction(actionDashed);
    menuView->addAction(actionShow_grid);
    menuView->addSeparator();
    menuView->addAction(actionZoom_in);
    menuView->addAction(actionZoom_out);
    menuView->addAction(actionZoom_reset);
    menuEdit->addAction(actionDelete);
    menuEdit->addAction(actionEdit);
    menuTools->addAction(actionNetlist);

    retranslateUi(this);

    QMetaObject::connectSlotsByName(this);

    actionShow_grid->setChecked(view->getShow_grid());
    catch_keystrokes = true;

    // Connect signals and slots.
    connect(view, SIGNAL(cursorMoved(QPoint)), this, SLOT(cursorMoved(QPoint)));
    connect(&worksheet.sender, SIGNAL(setStatusText(QString)), this, SLOT(setStatusText(QString)));
    connect(&worksheet.sender, SIGNAL(enableKeyboard(bool)), this, SLOT(enableKeyboard(bool)));
}

void MainWindow::retranslateUi(QMainWindow *MainWindow)
{
    MainWindow->setWindowTitle(QApplication::translate("MainWindow", "QScheme", Q_NULLPTR));
    actionWire->setText(QApplication::translate("MainWindow", "Wire", Q_NULLPTR));
    actionJunction->setText("Junction");
    actionEntry->setText("Entry");
    actionLabel->setText("Label");
    actionText->setText("Text");
    actionSheet->setText("Sheet");
    actionModulePort->setText("ModulePort");
    actionBus->setText("Bus");
    actionDashed->setText("Dashed Line");
    actionImage->setText("Image");
    actionComponent->setText("Get component");
    actionNetlist->setText("Create Netlist");
    actionDelete->setText("Delete object");
    actionEdit->setText("Edit parameters...");
    actionShow_grid->setText(QApplication::translate("MainWindow", "Show grid", Q_NULLPTR));
    actionZoom_in->setText(QApplication::translate("MainWindow", "Zoom in", Q_NULLPTR));
    actionZoom_out->setText(QApplication::translate("MainWindow", "Zoom out", Q_NULLPTR));
    actionZoom_reset->setText(QApplication::translate("MainWindow", "Zoom Reset", Q_NULLPTR));
    actionExit->setText(QApplication::translate("MainWindow", "Exit", Q_NULLPTR));
    actionSave->setText(QApplication::translate("MainWindow", "Save", Q_NULLPTR));
    actionNew->setText(QApplication::translate("MainWindow", "New", Q_NULLPTR));
    actionLoad->setText(QApplication::translate("MainWindow", "Load", Q_NULLPTR));
    menuFile->setTitle(QApplication::translate("MainWindow", "File", Q_NULLPTR));
    menuPlace->setTitle(QApplication::translate("MainWindow", "Place", Q_NULLPTR));
    menuView->setTitle(QApplication::translate("MainWindow", "View", Q_NULLPTR));
    menuTools->setTitle("Tools");
    menuEdit->setTitle("Edit");
}

MainWindow::~MainWindow()
{
    delete view;
    delete scrollArea;

    delete actionWire;
    delete actionBus;
    delete actionDashed;
    delete actionImage;
    delete actionComponent;
    delete actionJunction;
    delete actionText;
    delete actionLabel;
    delete actionSheet;
    delete actionModulePort;
    delete actionEntry;
    delete actionShow_grid;
    delete actionZoom_in;
    delete actionZoom_out;
    delete actionZoom_reset;
    delete actionExit;
    delete actionSave;
    delete actionNew;
    delete actionLoad;
    delete actionNetlist;
    delete actionDelete;
    delete actionEdit;
    delete menuFile;
    delete menuPlace;
    delete menuView;
    delete menuTools;
    delete menuEdit;
    delete menuBar;
    delete statusText;
    delete cursorPosition;
    delete statusBar;
    delete Library::getInstance();
}

void MainWindow::cursorMoved(QPoint pos)
{
    QString s = QString("X = %1, Y = %2").arg(pos.x()).arg(pos.y());
    cursorPosition->setText(s);
}

void MainWindow::setStatusText(QString s)
{
    statusText->setText(s);
}

void MainWindow::enableKeyboard(bool enable)
{
    catch_keystrokes = !enable;
}

void MainWindow::on_actionPut_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        worksheet.put();
        view->update();
    }
}

void MainWindow::on_actionEscape_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        worksheet.escape();
        view->update();
    } else {
        worksheet.setMode(Sheet::DRAW);
        statusText->setText("");
    }
}

void MainWindow::on_actionMode_triggered()
{
    worksheet.change();
    view->update();
}

void MainWindow::on_actionNew_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        worksheet.again();
        view->update();
    }
}

void MainWindow::on_actionWire_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        on_actionEscape_triggered();
        statusText->setText("Placing Wires. P=Put, N=New, /=Change mode, Esc=Ready");
        Wire* wire = new Wire(&worksheet, Wire::WIRE);
        wire->setLastPosition(view->getCursorCoordinates());
        worksheet.setActiveDrawable(wire);
    }
}

void MainWindow::on_actionBus_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        on_actionEscape_triggered();
        statusText->setText("Placing Busses. P=Put, N=New, /=Change mode, Esc=Ready");
        Wire* bus = new Wire(&worksheet, Wire::BUS);
        bus->setLastPosition(view->getCursorCoordinates());
        worksheet.setActiveDrawable(bus);
    }
}

void MainWindow::on_actionNetlist_triggered()
{
    QSettings settings;
    QString dir = settings.value("netlist_dir").toString();

    QString filename = QFileDialog::getSaveFileName(this, tr("Save netlist..."), dir, tr("netlist (*.net *.cmp)"));
    if (!filename.isEmpty()) {

        // Save pathname
        QFileInfo fi(filename);
        QString path = fi.absolutePath();
        QString basename = fi.baseName();
        settings.setValue("netlist_dir", path);

        // Create netlist
        Netlist netlist;
        worksheet.createNetlist(netlist);
        netlist.joinMatchingNets();

        // Save cmp file
        QString cmpfilename = path + QDir::separator() + basename + ".cmp";
        QFile cmpfile(cmpfilename);
        cmpfile.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream cmpout(&cmpfile);
        netlist.saveCmpFile(cmpout);
        cmpfile.close();

        // Save net file
        QString netfilename = path + QDir::separator() + basename + ".net";
        QFile netfile(netfilename);
        netfile.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&netfile);
        netlist.saveNetFile(out);
        netfile.close();

        QMessageBox::information(this, "Notification", "Netlist written.");
    }
}

void MainWindow::on_actionDelete_triggered()
{
    if (worksheet.getMode() == Sheet::DELETE) {
        worksheet.setMode(Sheet::DRAW);
        statusText->setText("Back to Drawing mode...");
    } else {
        worksheet.setMode(Sheet::DELETE);
        statusText->setText("Deleting. D=Delete, Esc=Ready");
    }
}

void MainWindow::on_actionEdit_triggered()
{
    if (worksheet.getMode() == Sheet::EDIT) {
        worksheet.setMode(Sheet::DRAW);
        statusText->setText("Back to Drawing mode...");
    } else {
        worksheet.setMode(Sheet::EDIT);
        statusText->setText("Editing. E=Edit, Esc=Ready");
    }
}

void MainWindow::on_actionDashed_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        on_actionEscape_triggered();
        statusText->setText("Placing Dashed Lines. P=Put, N=New, /=Change mode, Esc=Ready");
        Wire* line = new Wire(&worksheet, Wire::DASHED);
        line->setLastPosition(view->getCursorCoordinates());
        worksheet.setActiveDrawable(line);
    }
}

void MainWindow::on_actionImage_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        on_actionEscape_triggered();

        // Load file with image.
        QSettings settings;
        QString dir = settings.value("image_dir").toString();

        QString filename = QFileDialog::getOpenFileName(this, tr("Choose image file..."), dir, tr("images (*.png *.bmp *jpg)"));
        if (!filename.isEmpty()) {

            // Save pathname
            QFileInfo fi(filename);
            settings.setValue("image_dir", fi.absolutePath());

            Image* image = new Image(&worksheet);
            image->setLastPosition(view->getCursorCoordinates());
            image->load(filename);
            worksheet.setActiveDrawable(image);
            statusText->setText("Placing Images, P=Put, Esc=Abort");
            view->update();
        }
    }
}

void MainWindow::on_actionComponent_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        on_actionEscape_triggered();

        QSettings settings;
        QString libdir = QDir::currentPath() + QDir::separator() + "lib";
        QString dir = settings.value("component_dir", libdir).toString();

        QString filename = QFileDialog::getOpenFileName(this, tr("Get component..."), dir, tr("component (*.sbl)"));
        if (!filename.isEmpty()) {

            // Save pathname
            QFileInfo fi(filename);
            settings.setValue("component_dir", fi.absolutePath());

            Component* component = new Component(&worksheet);
            component->setLastPosition(view->getCursorCoordinates());
            if (component->load(filename)) {
                worksheet.setActiveDrawable(component);
                statusText->setText("Placing Component, P=Put, R=Rotate, M=Mirror, E=Edit, Esc=Abort");
                view->update();
            } else {
                delete component;
                statusText->setText("Error while loading component.");
            }
        }
    }
}

bool MainWindow::checkDirty()
{
    if (worksheet.getDirty()) {
        if (QMessageBox::question(this, "Are you sure?", "Abandon edits?", QMessageBox::Yes|QMessageBox::No) ==
                QMessageBox::StandardButton::No) {
            return false;
        }
    }
    return true;
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionNewDoc_triggered()
{
    if (checkDirty()) {
        worksheet.clear();
        Library::getInstance()->clear();
    }
}

void MainWindow::on_actionLoad_triggered()
{

    if (checkDirty()) {
        QSettings settings;
        QString dir = settings.value("load_dir").toString();

        QString filename = QFileDialog::getOpenFileName(this, "Load scheme...", dir, "scheme (*.sch)");
        if (!filename.isEmpty()) {

            // Save pathname
            QFileInfo fi(filename);
            settings.setValue("load_dir", fi.absolutePath());

            // Clear current worksheet and library.
            worksheet.clear();
            Library::getInstance()->clear();

            // Read file into string
            QString val;
            QFile file(filename);
            file.open(QIODevice::ReadOnly | QIODevice::Text);
            val = file.readAll();
            file.close();

            // Create JSON structure
            QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());

            if (doc.isObject()) {
                QJsonObject main = doc.object();
                if (main.value("Application").toString() == APPLICATION) {

                    // Load library
                    QJsonValue library = main.value("library");
                    foreach (QJsonValue obj, library.toArray()) {
                        Symbol* s = new Symbol();
                        s->fromJson(obj.toObject());
                        Library::getInstance()->addSymbol(s);
                    }

                    // Load sheet
                    QJsonValue sheet = main.value(worksheet.getClassName());
                    worksheet.fromJson(sheet);

                    // Everything is fine.
                    worksheet.setDirty(false);
                    view->update();
                    return;
                }
            }
            QMessageBox::information(this, "error", "Cannot read file. Wrong format.");
        }
    }
}

void MainWindow::on_actionSave_triggered()
{
    QSettings settings;
    QString dir = settings.value("save_dir").toString();

    QString filename = QFileDialog::getSaveFileName(this, tr("Save scheme..."), dir, tr("scheme (*.sch)"));
    if (!filename.isEmpty()) {

        // Save pathname
        QFileInfo fi(filename);
        settings.setValue("save_dir", fi.absolutePath());

        // Create json object
        QJsonObject obj;
        obj.insert("Application", QJsonValue(APPLICATION));
        obj.insert("Version", QJsonValue(VERSION));
        obj.insert("Url", QJsonValue(URL));
        obj.insert("Manufacturer", QJsonValue(MANUFACTURER));

        QJsonArray array;
        foreach (Symbol* symbol, Library::getInstance()->getAllSymbols()) {
            QJsonObject symboljson;
            symbol->toJson(symboljson);
            array.append(symboljson);
        }
        obj.insert("library", array);

        QJsonObject worksheetjson;
        worksheet.toJson(worksheetjson);
        obj.insert(worksheet.getClassName(), worksheetjson);

        QJsonDocument doc;
        doc.setObject(obj);

        // Save as json
        QFile file(filename);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        out << doc.toJson();
        file.close();

        statusText->setText(filename);
        worksheet.setDirty(false);
    }
}

void MainWindow::on_actionShow_grid_triggered(bool checked)
{
    view->setShow_grid(checked);
}

void MainWindow::on_actionZoom_in_triggered()
{
    double zoom = view->getZoom();
    view->setZoom(zoom * 1.25);
    scrollArea->widget()->resize(view->getSize());
}

void MainWindow::on_actionZoom_out_triggered()
{
    double zoom = view->getZoom();
    view->setZoom(zoom / 1.25);
    scrollArea->widget()->resize(view->getSize());
}

void MainWindow::on_actionZoom_reset_triggered()
{
    view->setZoom(1.0);
    scrollArea->widget()->resize(view->getSize());
}

void MainWindow::on_actionJunction_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        on_actionEscape_triggered();
        statusText->setText("Placing Junctions. P=Put, Esc=Abort");
        Junction* junction = new Junction(&worksheet);
        junction->setLastPosition(view->getCursorCoordinates());
        worksheet.setActiveDrawable(junction);
        view->update();
    }
}

void MainWindow::on_actionText_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
        bool ok;
        catch_keystrokes = false;
        QString value = QInputDialog::getText(this, "Placing Text", "Text:", QLineEdit::Normal, "", &ok);
        catch_keystrokes = true;
        if (ok && !value.isEmpty()) {
            on_actionEscape_triggered();
            statusText->setText("Placing Text. P=Put, R=Rotate, /=Change allignment, Esc=Abort");
            Text* text = new Text(&worksheet);
            text->setValue(value);
            text->setLastPosition(view->getCursorCoordinates());
            worksheet.setActiveDrawable(text);
            view->update();
        }
    }
}

void MainWindow::on_actionLabel_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
    }
}

void MainWindow::on_actionModulePort_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
    }
}

void MainWindow::on_actionEntry_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
    }
}

void MainWindow::on_actionRotate_triggered()
{
    worksheet.rotate();
    view->update();
}

void MainWindow::on_actionMirror_triggered()
{
    worksheet.mirror();
    view->update();
}

void MainWindow::on_actionSheet_triggered()
{
    if (worksheet.getMode() == Sheet::DRAW) {
    }
}

void MainWindow::on_actionUp_triggered()
{
    view->moveUp();
}

void MainWindow::on_actionDown_triggered()
{
    view->moveDown();
}

void MainWindow::on_actionLeft_triggered()
{
    view->moveLeft();
}

void MainWindow::on_actionRight_triggered()
{
    view->moveRight();
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (catch_keystrokes) {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            switch (keyEvent->key()) {

            // Cursor move commands
            case Qt::Key_Up:
                on_actionUp_triggered();
                break;
            case Qt::Key_Down:
                on_actionDown_triggered();
                break;
            case Qt::Key_Left:
                on_actionLeft_triggered();
                break;
            case Qt::Key_Right:
                on_actionRight_triggered();
                break;

            // Place item commands
            case Qt::Key_W:
                on_actionWire_triggered();
                break;
            case Qt::Key_B:
                on_actionBus_triggered();
                break;
            case Qt::Key_D:
                if (worksheet.getMode() == Sheet::DELETE) {
                    worksheet.deleteItem();
                    view->update();
                }
                else on_actionDelete_triggered();
                break;
            case Qt::Key_I:
                on_actionImage_triggered();
                break;
            case Qt::Key_G:
                on_actionComponent_triggered();
                break;
            case Qt::Key_J:
                on_actionJunction_triggered();
                break;
            case Qt::Key_T:
                on_actionText_triggered();
                break;
            case Qt::Key_L:
                on_actionLabel_triggered();
                break;
            case Qt::Key_S:
                on_actionSheet_triggered();
                break;
            /*case Qt::Key_M:
                on_actionModulePort_triggered();
                break;*/
            case Qt::Key_R:
                on_actionRotate_triggered();
                break;
            case Qt::Key_M:
                on_actionMirror_triggered();
                break;

            // Drawing commands
            case Qt::Key_E:
                if ((worksheet.getMode() == Sheet::EDIT) || (worksheet.getActiveDrawable())) {
                    catch_keystrokes = false;
                    worksheet.edit();
                    catch_keystrokes = true;
                    view->update();
                } else {
                    on_actionEdit_triggered();
                }
                break;
            case Qt::Key_P:
                on_actionPut_triggered();
                break;
            case Qt::Key_N:
                on_actionNew_triggered();
                break;
            case Qt::Key_Slash:
                on_actionMode_triggered();
                break;
            case Qt::Key_Escape:
                on_actionEscape_triggered();
                break;

            // Zoom commands
            case Qt::Key_0:
                on_actionZoom_reset_triggered();
                break;
            case Qt::Key_Plus:
                on_actionZoom_in_triggered();
                break;
            case Qt::Key_Minus:
                on_actionZoom_out_triggered();
                break;

            // Other
            default:
                qDebug("Key press %d", keyEvent->key());
                break;
            }
            return true;
        } else {
            // standard event processing
            return QObject::eventFilter(obj, event);
        }
    } else {
        return QObject::eventFilter(obj, event);
    }

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (checkDirty()) event->accept();
    else event->ignore();
}

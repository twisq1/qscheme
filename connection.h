#ifndef CONNECTION_H
#define CONNECTION_H

#include <QGlobal.h>
#include <QSet>
#include "drawable.h"
#include "coordinates.h"

class Sheet;

class Connection : public Drawable
{
public:
    Connection();
    Connection(Sheet* sheet);

    // New functions
    virtual bool isConnectedTo(Coordinates p) = 0;
    virtual QSet<Coordinates> getCoordinates() = 0;

    // Overrides
    virtual bool isPresentAt(const Coordinates &p);
};

#endif // CONNECTION_H

#ifndef COMPONENT_H
#define COMPONENT_H

#include <QPixmap>
#include <QList>
#include <QRegularExpression>
#include "symbol.h"
#include "coordinates.h"
#include "pin.h"
#include "drawable.h"

class Component : public Drawable
{
    static const int TEXT_COLOR = 0x0000ff;
    static const int COMP_COLOR = 0xff0000;

public:
    Component(Sheet* sheet);
    virtual ~Component();

    virtual const QString getClassName() { return "component"; }
    virtual void toJson(QJsonObject& obj);
    virtual bool fromJson(const QJsonValue &json);
    virtual bool isPresentAt(const Coordinates &p);

    // Load component from file.
    bool load(const QString& filename);

    // Overrides
    virtual void draw(QProjectView* view);
    virtual void put();
    virtual void rotate();
    virtual void mirror();
    virtual void edit();

    // Convenience functions
    QList<Pin*> getPins();
    void calculatePinPoint(const Pin& pin, Coordinates *base, Coordinates *end, Pin::Side *side);

    QString getReference() const;
    QString getValue() const;
    QString getShape() const;

private:
    Symbol* symbol;
    QString reference;
    QString shape;
    QString value;
    QString ordercode;
    double price;
    QPointF referenceDisplacement;
    QPointF valueDisplacement;

    // Regexes for load function
    static QRegularExpression re_name;
    static QRegularExpression re_referance;
    static QRegularExpression re_shape;
    static QRegularExpression re_sizes;
    static QRegularExpression re_line;
    static QRegularExpression re_pin;
    static QRegularExpression re_value;
    static QRegularExpression re_ordercode;
    static QRegularExpression re_price;
    static QRegularExpression re_vector;

    // Helper functions
    void parseLine(const QString& line);
    Pin::Side getPinSide(const Pin& pin);
    QPoint getSize() const;
    void placeRefAndValue();
};

#endif // COMPONENT_H

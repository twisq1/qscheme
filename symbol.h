#ifndef SYMBOL_H
#define SYMBOL_H

#include <QPixmap>
#include <QList>
#include <QString>
#include <QJsonObject>
#include <QJsonValue>
#include <QPoint>
#include "pin.h"

class Symbol
{
public:
    Symbol();
    virtual ~Symbol();

    // Hide copy constructor and operator
    Symbol(Symbol const &) = delete;
    void operator=(Symbol const &x) = delete;

    void toJson(QJsonObject& obj);
    bool fromJson(const QJsonValue& json);

    QPoint getSize() const;
    void setSize(const QPoint &value);

    QString getName() const;
    void setName(const QString &value);

    QList<Pin *> getPins() const;
    void addPin(Pin* p);

    QPixmap *getPixmap() const;
    void setPixmap(QPixmap *value);

    QString getReference() const;
    void setReference(const QString &value);

    QString getValue() const;
    void setValue(const QString &value);

    QString getShape() const;
    void setShape(const QString &value);

    QString getOrdercode() const;
    void setOrdercode(const QString &value);

    double getPrice() const;
    void setPrice(double value);

private:
    QPixmap* pixmap;
    QPoint size;
    QList<Pin*> pins;
    QString name;
    QString reference;
    QString value;
    QString shape;
    QString ordercode;
    double price;
};

#endif // SYMBOL_H

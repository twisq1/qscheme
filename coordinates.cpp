#include "sheet.h"
#include "math.h"
#include "coordinates.h"

// Poor mans hash function
uint qHash(const Coordinates &coordinates) {
    uint decix = coordinates.x() * 10;
    uint deciy = coordinates.y() * 10;
    return (10000 * deciy + decix);
}

Coordinates::Coordinates()
{
    has_value = false;
}

Coordinates::Coordinates(int x, int y)
{
    setX(x);
    setY(y);
}

Coordinates::Coordinates(double x, double y)
{
    setX(x);
    setY(y);
}

Coordinates::Coordinates(QPointF p)
{
    setX(p.x());
    setY(p.y());
}

Coordinates &Coordinates::operator=(QPointF p)
{
    setX(p.x());
    setY(p.y());
    return *this;
}

Coordinates Coordinates::snapToGrid()
{
    setX(round(x()));
    setY(round(y()));
    return *this;
}

bool Coordinates::hasValue() const
{
    return has_value;
}

void Coordinates::setX(qreal x)
{
    QPointF::setX(x);
    has_value = true;
}

void Coordinates::setY(qreal y)
{
    QPointF::setY(y);
    has_value = true;
}

bool Coordinates::getOn_sheet(Sheet* sheet) const
{
    return (x() >= 0) && (x() <= sheet->getSize().x()) && (y() >= 0) && (y() <= sheet->getSize().y());
}

#include <QDebug>
#include <QMessageBox>
#include "sheet.h"
#include "component.h"
#include "image.h"
#include "junction.h"
#include "text.h"
#include "wire.h"
#include "drawable.h"

Drawable::Drawable(Sheet* sheet)
    : sheet(sheet)
{
    angle = 0;
    mirrored = false;
}

Drawable::~Drawable()
{
}

Drawable *Drawable::create(QString type, Sheet* sheet)
{
    if (type == "component") return new Component(sheet);
    else if (type == "image") return new Image(sheet);
    else if (type == "junction") return new Junction(sheet);
    else if (type == "sheet") return new Sheet(sheet);
    else if (type == "text") return new Text(sheet);
    else if (type == "wire") return new Wire(sheet);
    qWarning() << "Illegal drawable type: " << type;
    return nullptr;
}

void Drawable::toJson(QJsonObject& obj)
{
    obj.insert("posx", lastPosition.x());
    obj.insert("posy", lastPosition.y());
    obj.insert("angle", angle);
    obj.insert("mirrored", mirrored);
}

bool Drawable::fromJson(const QJsonValue &json)
{
    if (json.isObject()) {
        QJsonObject obj = json.toObject();
        lastPosition.setX(obj.value("posx").toDouble());
        lastPosition.setY(obj.value("posy").toDouble());
        angle = obj.value("angle").toInt();
        mirrored = obj.value("mirrored").toBool();
        return true;
    }
    return false;
}

bool Drawable::isPresentAt(const Coordinates &p)
{
    return (lastPosition == p);
}

Coordinates Drawable::getLastPosition() const
{
    return lastPosition;
}

void Drawable::setLastPosition(const Coordinates &value)
{
    lastPosition = value;
}

void Drawable::setDirty(bool value)
{
    Q_ASSERT_X(sheet, "setDirty", "No sheet for drawable");
    sheet->setDirty(value);
}

void Drawable::change()
{
    qDebug() << "Change";
}

void Drawable::rotate()
{
    angle = (angle + 90) % 360;
    setDirty();
}

void Drawable::mirror()
{
    mirrored = !mirrored;
    setDirty();
}

void Drawable::edit()
{
    QMessageBox::information(nullptr, "Notification", "Object has no editable parameters.");
}

void Drawable::escape()
{
    sheet->removeActiveDrawable();
    delete this;
}

void Drawable::put()
{
    qDebug() << "Put";
}

void Drawable::end()
{
    qDebug() << "End";
}

void Drawable::again()
{
    qDebug() << "Again";
}

void Drawable::leftClick(const Coordinates &position)
{
    lastPosition = position;
    put();
}

void Drawable::rightClick(const Coordinates &position)
{
    Q_UNUSED(position);
    escape();
}

void Drawable::cursorMoved(const Coordinates &position)
{
    lastPosition = position;
}

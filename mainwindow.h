#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScrollArea>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QLabel>
#include <QEvent>
#include "sheet.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void cursorMoved(QPoint pos);
    void setStatusText(QString s);
    void enableKeyboard(bool enable);

private slots:
    void on_actionExit_triggered();
    void on_actionSave_triggered();
    void on_actionLoad_triggered();
    void on_actionNewDoc_triggered();
    void on_actionWire_triggered();
    void on_actionDashed_triggered();
    void on_actionImage_triggered();
    void on_actionComponent_triggered();
    void on_actionShow_grid_triggered(bool checked);
    void on_actionZoom_in_triggered();
    void on_actionZoom_out_triggered();
    void on_actionZoom_reset_triggered();
    void on_actionJunction_triggered();
    void on_actionText_triggered();
    void on_actionLabel_triggered();
    void on_actionModulePort_triggered();
    void on_actionSheet_triggered();
    void on_actionBus_triggered();
    void on_actionNetlist_triggered();
    void on_actionDelete_triggered();
    void on_actionEdit_triggered();

protected:
    // The eventfilter is overriden to catch key strokes.
    bool eventFilter(QObject *obj, QEvent *event);
    void closeEvent(QCloseEvent *event);

private:
    Sheet worksheet;
    QScrollArea *scrollArea;
    QProjectView* view;

    QAction *actionWire;
    QAction *actionBus;
    QAction *actionDashed;
    QAction *actionImage;
    QAction *actionComponent;
    QAction *actionJunction;
    QAction *actionText;
    QAction *actionEntry;
    QAction *actionSheet;
    QAction *actionLabel;
    QAction *actionModulePort;
    QAction *actionShow_grid;
    QAction *actionZoom_in;
    QAction *actionZoom_out;
    QAction *actionZoom_reset;
    QAction *actionExit;
    QAction *actionSave;
    QAction *actionNew;
    QAction *actionLoad;
    QAction *actionNetlist;
    QAction *actionDelete;
    QAction *actionEdit;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuPlace;
    QMenu *menuView;
    QMenu *menuTools;
    QMenu *menuEdit;
    QStatusBar *statusBar;
    QLabel *statusText;
    QLabel *cursorPosition;
    bool catch_keystrokes;

    void retranslateUi(QMainWindow *MainWindow);
    void on_actionUp_triggered();
    void on_actionDown_triggered();
    void on_actionLeft_triggered();
    void on_actionRight_triggered();
    void on_actionPut_triggered();
    void on_actionEscape_triggered();
    void on_actionMode_triggered();
    void on_actionNew_triggered();
    void on_actionEntry_triggered();
    void on_actionRotate_triggered();
    void on_actionMirror_triggered();

    bool checkDirty();

};

#endif // MAINWINDOW_H

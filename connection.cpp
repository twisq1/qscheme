#include "connection.h"

Connection::Connection()
    : Drawable(nullptr)
{
}

Connection::Connection(Sheet *sheet)
    : Drawable(sheet)
{
}

bool Connection::isPresentAt(const Coordinates &p)
{
    return isConnectedTo(p);
}

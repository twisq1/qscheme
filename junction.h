#ifndef JUNCTION_H
#define JUNCTION_H

#include "connection.h"

class QProjectView;

class Junction : public Connection
{
    static constexpr double JUNCTION_WIDTH = 0.2;
    static const int JUNCTION_COLOR = Qt::black;

public:
    Junction(Sheet* sheet);

    virtual const QString getClassName() { return "junction"; }

    // Overrides
    virtual void draw(QProjectView* view);
    virtual void put();

    virtual bool isConnectedTo(Coordinates p);
    virtual QSet<Coordinates> getCoordinates();
};

#endif // JUNCTION_H

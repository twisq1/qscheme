#ifndef IMAGE_H
#define IMAGE_H

#include <QPixmap>
#include "coordinates.h"
#include "symbol.h"
#include "drawable.h"

class Image : public Drawable
{
public:
    Image(Sheet* sheet);
    virtual ~Image();

    virtual const QString getClassName() { return "image"; }
    virtual void toJson(QJsonObject& obj);
    virtual bool fromJson(const QJsonValue &json);

    void load(const QString filename);

    // Overrides
    virtual void draw(QProjectView* view);
    virtual void put();

private:
    Symbol* symbol;
};

#endif // IMAGE_H

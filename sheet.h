#ifndef SHEET_H
#define SHEET_H

#include <QVector>
#include <QPoint>
#include <QObject>
#include "sender.h"
#include "netlist.h"
#include "drawable.h"

class QProjectView;

class Sheet : public Drawable
{
    static const int BORDER_WIDTH = 3;
    static const int BORDER_COLOR = 0x0000ff;
    static const int SHEET_COLOR = 0xffffff;

public:

    enum MODE {
        DRAW,
        EDIT,
        DELETE
    };

    Sheet(Sheet* parent = nullptr);
    virtual ~Sheet();

    void clear();

    virtual const QString getClassName() { return "sheet"; }
    virtual void toJson(QJsonObject& obj);
    virtual bool fromJson(const QJsonValue &json);

    QPoint getSize() const;
    void setActiveDrawable(Drawable *value);
    void removeActiveDrawable();
    Drawable *getActiveDrawable() const;
    void addDrawable(Drawable* item);
    void createNetlist(Netlist& netlist);

    void deleteItem();

    // Overrides
    virtual void draw(QProjectView* view);
    virtual void escape();
    virtual void change();
    virtual void put();
    virtual void end();
    virtual void again();
    virtual void rotate();
    virtual void mirror();
    virtual void edit();
    virtual void leftClick(const Coordinates &position);
    virtual void rightClick(const Coordinates &position);
    virtual void cursorMoved(const Coordinates &position);

    Sender sender;

    bool getDirty() const;
    void setDirty(bool value = true);

    void setMode(const MODE &value);
    MODE getMode() const;

private:
    QPoint size;                // Worksheet size

    QVector<Drawable*> items;   // All items on sheet

    Drawable* activeDrawable;   // Current active drawable item

    bool dirty;

    Coordinates cursor;         // Position of cursor on sheet
    MODE mode;
};

#endif // SHEET_H

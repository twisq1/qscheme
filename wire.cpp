#include <QJsonArray>
#include "qprojectview.h"
#include "wire.h"

Wire::Wire(Sheet* sheet, LineType type)
    : Connection(sheet)
{
    mode_last_segment = POINT;
    indent = false;
    line_type = type;
}

Wire::~Wire()
{
}

void Wire::toJson(QJsonObject &obj)
{
    switch (line_type) {
    case WIRE:
        obj.insert("type", "wire");
        break;
    case BUS:
        obj.insert("type", "bus");
        break;
    case DASHED:
        obj.insert("type", "line");
        break;
    default:
        obj.insert("type", "none");
        break;
    }
    QJsonArray array;
    foreach(Coordinates p, points) {
        QJsonObject pjson;
        pjson.insert("x", p.x());
        pjson.insert("y", p.y());
        array.append( pjson);
    }
    obj.insert("points", array);
}

bool Wire::fromJson(const QJsonValue &json)
{
    QJsonObject obj = json.toObject();
    QString lt = obj.value("type").toString();
    if (lt == "wire") line_type = WIRE;
    else if (lt == "bus") line_type = BUS;
    else if (lt == "line") line_type = DASHED;
    else line_type = NONE;

    foreach (const QJsonValue pvalue, obj.value("points").toArray()) {
        double x = pvalue.toObject().value("x").toDouble();
        double y = pvalue.toObject().value("y").toDouble();
        Coordinates p(x, y);
        points.push_back(p);
    }

    return true;
}

void Wire::draw(QProjectView* view)
{
    if (!points.size()) return;

    int width;
    int color;
    int indent_color;
    int style;

    switch (line_type) {
    case WIRE:
        width = WIRE_SIZE;
        color = WIRE_COLOR;
        indent_color = WIRE_INDENT_COLOR;
        style = WIRE_STYLE;
        break;
    case BUS:
        width = BUS_SIZE;
        color = BUS_COLOR;
        indent_color = BUS_INDENT_COLOR;
        style = BUS_STYLE;
        break;
    case DASHED:
        width = DASHED_SIZE;
        color = DASHED_COLOR;
        indent_color = DASHED_INDENT_COLOR;
        style = DASHED_STYLE;
        break;
    default:
        qFatal("Wire::draw, Illegal wire type");
        break;
    }
    if (indent) color = indent_color;

    Coordinates walker;
    foreach (Coordinates point, points) {
        if (walker.hasValue()) {
            view->drawLine(walker, point, width, color, style);
        }
        walker = point;
    }

    Coordinates intermediate;
    switch (mode_last_segment) {
    case HORIZONTAL_VERTICAL:
        intermediate.setX(lastPosition.x());
        intermediate.setY(walker.y());
        view->drawLine(walker, intermediate, width, color, style);
        view->drawLine(intermediate, lastPosition, width, color, style);
        break;
    case VERTICAL_HORIZONTAL:
        intermediate.setX(walker.x());
        intermediate.setY(lastPosition.y());
        view->drawLine(walker, intermediate, width, color, style);
        view->drawLine(intermediate, lastPosition, width, color, style);
        break;
    case DIRECT:
        view->drawLine(walker, lastPosition, width, color, style);
        break;
    case POINT:
        view->drawPoint(walker, width, color);
        break;
    case CLOSED:
        break;
    }
}

void Wire::end()
{
    Q_ASSERT_X(sheet, "Wire::end", "No sheet");

    close();
    sheet->addDrawable(this);
    sheet->removeActiveDrawable();
}

void Wire::again()
{
    Q_ASSERT_X(sheet, "Wire::end", "No sheet");

    close();
    sheet->addDrawable(this);
    Wire* newWire = new Wire(sheet, line_type);
    newWire->setLastPosition(getLastPosition());
    sheet->setActiveDrawable(newWire);
}

void Wire::cursorMoved(const Coordinates &position)
{
    if (points.size() && (mode_last_segment == POINT)) {
        if (position.x() != lastPosition.x()) mode_last_segment = HORIZONTAL_VERTICAL;
        else if (position.y() != lastPosition.y()) mode_last_segment = VERTICAL_HORIZONTAL;
    }
    Drawable::cursorMoved(position);
}

bool Wire::isConnectedTo(Coordinates p)
{
    // Only wires can make connections.
    if (line_type != WIRE) return false;

    Coordinates lastpoint;
    foreach (Coordinates point, points) {
        if (p == point) return true;
        if (lastpoint.hasValue() && (lastpoint != point)) {

            // Check if p is on the line between point and lastpoint.
            double xmin = qMin(point.x(), lastpoint.x());
            double xmax = qMax(point.x(), lastpoint.x());
            double ymin = qMin(point.y(), lastpoint.y());
            double ymax = qMax(point.y(), lastpoint.y());
            if (xmin == xmax) {

                // Vertical line. Check if y is between top and bottom.
                if ((p.x() == xmin) && (p.y() >= ymin) && (p.y() <= ymax)) return true;
            } else {

                // Horizontal or diagonal line. Check if y has expected value.
                if ((p.x() >= xmin) && (p.x() <= xmax)) {
                    double ytarget = point.y() + (lastpoint.y() - point.y()) * (p.x() - point.x()) / (lastpoint.x() - point.x());
                    if (ytarget == p.y()) return true;
                }
            }
        }
        lastpoint = point;
    }
    return false;
}

QSet<Coordinates> Wire::getCoordinates()
{
    QSet<Coordinates> list;
    list.insert(points.first());
    list.insert(points.last());
    return list;
}

void Wire::put()
{
    if (!points.size()) {
        points.append(lastPosition);
        setIndent(true);
    } else {
        Coordinates start = points.last();

        Coordinates intermediate;
        switch (mode_last_segment) {
        case HORIZONTAL_VERTICAL:
            intermediate.setX(lastPosition.x());
            intermediate.setY(start.y());
            points.append(intermediate);
            points.append(lastPosition);
            mode_last_segment = POINT;
            break;
        case VERTICAL_HORIZONTAL:
            intermediate.setX(start.x());
            intermediate.setY(lastPosition.y());
            points.append(intermediate);
            points.append(lastPosition);
            mode_last_segment = POINT;
            break;
        case DIRECT:
            points.append(lastPosition);
            break;
        case POINT:
        case CLOSED:
            return;
        }
    }
}

void Wire::change()
{
    switch (mode_last_segment) {
    case HORIZONTAL_VERTICAL:
        mode_last_segment = VERTICAL_HORIZONTAL;
        break;
    case VERTICAL_HORIZONTAL:
        mode_last_segment = DIRECT;
        break;
    case DIRECT:
        mode_last_segment = HORIZONTAL_VERTICAL;
        break;
    case POINT:
    case CLOSED:
        break;
    }
}

void Wire::escape()
{
    end();
}

void Wire::leftClick(const Coordinates &position)
{
    setLastPosition(position);
    put();
}

void Wire::rightClick(const Coordinates &position)
{
    Q_UNUSED(position);
    end();
}

void Wire::setIndent(bool value)
{
    indent = value;
}

void Wire::close()
{
    put();
    mode_last_segment = CLOSED;
    setIndent(false);
}

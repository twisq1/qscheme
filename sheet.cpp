#include <QJsonArray>
#include <QMap>
#include <QDebug>
#include "qprojectview.h"
#include "sheet.h"

Sheet::Sheet(Sheet* parent)
    : Drawable(parent)
{
    size.setX(100);
    size.setY(80);
    activeDrawable = nullptr;
    dirty = false;
    mode = DRAW;
}

void Sheet::draw(QProjectView* view)
{
    view->drawRectangle(Coordinates(0,0), Coordinates(size), BORDER_WIDTH, BORDER_COLOR, SHEET_COLOR);

    foreach(Drawable* item, items) {
        item->draw(view);
    }

    if (activeDrawable) {
        activeDrawable->draw(view);
    }
}

Sheet::~Sheet()
{
    clear();
}

void Sheet::clear()
{
    foreach (Drawable* item, items) {
        delete item;
    }
    items.clear();
    if (activeDrawable) {
        delete activeDrawable;
        activeDrawable = nullptr;
    }
    setDirty(false);
}

void Sheet::toJson(QJsonObject& obj)
{
    obj.insert("sizex", QJsonValue(size.x()));
    obj.insert("sizey", QJsonValue(size.y()));

    QMap<QString, QJsonArray*> arrays;
    foreach (Drawable* item, items) {
        if (!arrays.contains(item->getClassName())) {
            arrays.insert(item->getClassName(), new QJsonArray());
        }
        QJsonArray* parray = arrays.value(item->getClassName());
        QJsonObject itemjson;
        item->toJson(itemjson);
        parray->append(itemjson);
    }

    foreach (QString s, arrays.keys()) {
        QJsonArray* parray = arrays.value(s);
        obj.insert(s, *parray);
        delete parray;
    }
}

bool Sheet::fromJson(const QJsonValue &json)
{
    QJsonObject obj = json.toObject();
    foreach(const QString key, obj.keys()) {
        if (key == "sizex") size.setX(obj.value(key).toInt());
        else if (key == "sizey") size.setY(obj.value(key).toInt());
        else {
            QJsonArray array = obj.value(key).toArray();
            foreach (const QJsonValue value, array) {
                Drawable* drawable = Drawable::create(key, this);
                if (!drawable) {
                    qWarning() << "Illegal drawable type: " << key;
                } else {
                    drawable->fromJson(value);
                    addDrawable(drawable);
                }
            }
        }
    }
    return true;
}

QPoint Sheet::getSize() const
{
    return size;
}

void Sheet::addDrawable(Drawable* item)
{
    items.append(item);
    setDirty();
}

void Sheet::createNetlist(Netlist &netlist)
{
    foreach (Drawable* item, items) {
        netlist.addDrawable(item);
    }
}

void Sheet::deleteItem()
{
    QVector<Drawable*>::iterator it = items.begin();
    while (it != items.end()) {
        if ((*it)->isPresentAt(cursor)) {
            delete (*it);
            items.erase(it);
            setDirty();
            return;
        }
        it++;
    }
}

void Sheet::setActiveDrawable(Drawable *value)
{
    activeDrawable = value;
}

void Sheet::removeActiveDrawable()
{
    activeDrawable = nullptr;
    sender.send("");
}

void Sheet::again()
{
    if (activeDrawable) activeDrawable->again();
}

void Sheet::rotate()
{
    if (activeDrawable) activeDrawable->rotate();
}

void Sheet::mirror()
{
    if (activeDrawable) activeDrawable->mirror();
}

void Sheet::edit()
{
    if (activeDrawable) activeDrawable->edit();
    else {
        foreach (Drawable* item, items) {
            if (item->isPresentAt(cursor)) {
                item->edit();
                return;
            }
        }
    }
}

void Sheet::leftClick(const Coordinates &position)
{
    switch (mode) {
    case DELETE:
        deleteItem();
        break;
    case EDIT:
        sender.enableKeyboard(true);
        edit();
        sender.enableKeyboard(false);
        break;
    default:
        if (activeDrawable) activeDrawable->leftClick(position);
        break;
    }
}

void Sheet::rightClick(const Coordinates &position)
{
    if (activeDrawable) activeDrawable->rightClick(position);
}

void Sheet::put()
{
    if (activeDrawable) activeDrawable->put();
}

void Sheet::change()
{
    if (activeDrawable) activeDrawable->change();
}

void Sheet::escape()
{
    if (activeDrawable) activeDrawable->escape();
}

void Sheet::end()
{
    if (activeDrawable) activeDrawable->end();
}

void Sheet::cursorMoved(const Coordinates &position)
{
    if (activeDrawable) activeDrawable->cursorMoved(position);
    else cursor = position;
}

Drawable *Sheet::getActiveDrawable() const
{
    return activeDrawable;
}

bool Sheet::getDirty() const
{
    return dirty;
}

void Sheet::setDirty(bool value)
{
    dirty = value;
}

void Sheet::setMode(const MODE &value)
{
    mode = value;
}

Sheet::MODE Sheet::getMode() const
{
    return mode;
}

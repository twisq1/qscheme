#include <QPainter>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QRegularExpressionMatch>
#include <QFileInfo>
#include <QDebug>
#include "qprojectview.h"
#include "symbol.h"
#include "library.h"
#include "editcomponentdialog.h"
#include "component.h"

// Regexes for loader
//QRegularExpression Component::re_name("^'([^']*)'");
QRegularExpression Component::re_referance("^REFERENCE +'([^']*)'");
QRegularExpression Component::re_shape("^SHAPE +'([^']*)'");
QRegularExpression Component::re_value("^VALUE +'([^']*)'");
QRegularExpression Component::re_ordercode("^ORDERCODE +'([^']*)'");
QRegularExpression Component::re_price("^PRICE +'([^']*)'");
QRegularExpression Component::re_vector("^VECTOR");
QRegularExpression Component::re_sizes("^\\{X Size =\\} +([0-9]+) +\\{Y Size =\\} +([0-9]+)");
QRegularExpression Component::re_line("^LINE +\\+([0-9\\.]+) +\\+([0-9\\.]+) +\\+([0-9\\.]+) +\\+([0-9\\.]+)");
QRegularExpression Component::re_pin("^([TBLR][0-9]+) +([0-9]+) +([A-Z]+) +([A-Z]+) +'([^']*)'");

Component::Component(Sheet* sheet)
    : Drawable(sheet)
{
    symbol = nullptr;
    reference = "";
    shape = "";
    value = "";
    ordercode = "";
    price = 0.0;
}

Component::~Component()
{
}

void Component::toJson(QJsonObject& obj)
{
    Q_ASSERT_X(symbol, "toJson", "Component has no symbol.");

    Drawable::toJson(obj);
    obj.insert("symbol", symbol->getName());
    obj.insert("reference", reference);
    obj.insert("value", value);
    obj.insert("ordercode", ordercode);
    obj.insert("price", price);
    obj.insert("shape", shape);
    QJsonObject refdis;
    refdis.insert("x", referenceDisplacement.x());
    refdis.insert("y", referenceDisplacement.y());
    obj.insert("referenceDisplacement", refdis);
    QJsonObject valuedis;
    valuedis.insert("x", valueDisplacement.x());
    valuedis.insert("y", valueDisplacement.y());
    obj.insert("valueDisplacement", valuedis);
}

bool Component::fromJson(const QJsonValue &json)
{
    if (Drawable::fromJson(json)) {
        QJsonObject obj = json.toObject();
        reference = obj.value("reference").toString();
        shape = obj.value("shape").toString();
        value = obj.value("value").toString();
        ordercode = obj.value("ordercode").toString();
        price = obj.value("price").toDouble();
        QJsonObject refdis = obj.value("referenceDisplacement").toObject();
        referenceDisplacement.setX(refdis.value("x").toDouble());
        referenceDisplacement.setY(refdis.value("y").toDouble());
        QJsonObject valuedis = obj.value("valueDisplacement").toObject();
        valueDisplacement.setX(valuedis.value("x").toDouble());
        valueDisplacement.setY(valuedis.value("y").toDouble());
        QString symbolname = obj.value("symbol").toString();
        symbol = Library::getInstance()->getSymbol(symbolname);
        return true;
    }
    return false;
}

bool Component::isPresentAt(const Coordinates &p)
{
    if ((p.x() >= lastPosition.x()) && (p.x() <= lastPosition.x() + symbol->getSize().x()) &&
        (p.y() >= lastPosition.y()) && (p.y() <= lastPosition.y() + symbol->getSize().y()))
        return true;
    return false;
}

void Component::draw(QProjectView *view)
{
    Q_ASSERT_X(symbol, "draw", "Component has no symbol.");

    if (lastPosition.hasValue() && lastPosition.getOn_sheet(sheet)) {
        Coordinates valuePos(lastPosition.x() + valueDisplacement.x(), lastPosition.y() + valueDisplacement.y());
        Coordinates refPos(lastPosition.x() + referenceDisplacement.x(), lastPosition.y() + referenceDisplacement.y());
        view->drawText(refPos, reference, 0, false, 1.0, TEXT_COLOR);
        view->drawText(valuePos, value, 0, false, 1.0, TEXT_COLOR);
        if (symbol->getPixmap()) {
            QPixmap pm = symbol->getPixmap()->transformed(QTransform().rotate(angle));
            if (mirrored) {
                pm = pm.transformed(QTransform().scale(-1.0, 1.0));
            }
            view->drawImage(lastPosition, pm);
        } else {
            Coordinates otherSide(lastPosition.x() + getSize().x(), lastPosition.y() + getSize().y());
            view->drawRectangle(lastPosition, otherSide, 1, COMP_COLOR);
        }

        foreach (Pin* pin, symbol->getPins()) {
            Coordinates base;
            Pin::Side side;
            Coordinates end;
            calculatePinPoint(*pin, &base, &end, &side);
            view->drawLine(base, end, 1, COMP_COLOR);

            // Print pin name
            if (!symbol->getPixmap()) {
                Coordinates ppos;
                switch(side) {
                case Pin::TOP:
                    ppos.setX(base.x() - 0.5);
                    ppos.setY(base.y() + 0.5);
                    view->drawText(ppos, pin->getName(), 90, true, 1.0, TEXT_COLOR);
                    break;
                case Pin::BOTTOM:
                    ppos.setX(base.x() - 0.5);
                    ppos.setY(base.y() - 0.5);
                    view->drawText(ppos, pin->getName(), 90, false, 1.0, TEXT_COLOR);
                    break;
                case Pin::LEFT:
                    ppos.setX(base.x() + 0.5);
                    ppos.setY(base.y() - 0.5);
                    view->drawText(ppos, pin->getName(), 0, false, 1.0, TEXT_COLOR);
                    break;
                case Pin::RIGHT:
                    ppos.setX(base.x() - 0.5);
                    ppos.setY(base.y() - 0.5);
                    view->drawText(ppos, pin->getName(), 0, true, 1.0, TEXT_COLOR);
                    break;
                default:
                    qCritical("Illegal pin position.");
                    break;
                }

                // Print pin number
                if (!symbol->getPixmap() && !pin->getName().isEmpty()) {
                    QString number = QString::number(pin->getNumber());
                    Coordinates ppos;
                    switch(side) {
                    case Pin::TOP:
                        ppos.setX(base.x() - 1.0);
                        ppos.setY(base.y() - 0.5);
                        view->drawText(ppos, number, 90, false, 1.0, TEXT_COLOR);
                        break;
                    case Pin::BOTTOM:
                        ppos.setX(base.x() - 1.0);
                        ppos.setY(base.y() + 0.5);
                        view->drawText(ppos, number, 90, true, 1.0, TEXT_COLOR);
                        break;
                    case Pin::LEFT:
                        ppos.setX(base.x() - 0.5);
                        ppos.setY(base.y() - 1.0);
                        view->drawText(ppos, number, 0, true, 1.0, TEXT_COLOR);
                        break;
                    case Pin::RIGHT:
                        ppos.setX(base.x() + 0.5);
                        ppos.setY(base.y() - 1.0);
                        view->drawText(ppos, number, 0, false, 1.0, TEXT_COLOR);
                        break;
                    default:
                        qCritical("Illegal pin position.");
                        break;
                    }
                }
            }
        }
    }
}

void Component::put()
{
    sheet->addDrawable(this);
    sheet->setActiveDrawable(new Component(*this));
}

void Component::rotate()
{
    Drawable::rotate();
    placeRefAndValue();
}

void Component::mirror()
{
    Drawable::mirror();
    placeRefAndValue();
}

void Component::edit()
{
    EditComponentDialog dlg;
    dlg.setReference(reference);
    dlg.setValue(value);
    dlg.setShape(shape);
    dlg.setOrdercode(ordercode);
    dlg.setPrice(price);
    if (QDialog::Accepted == dlg.exec()) {
        reference = dlg.getReference();
        value = dlg.getValue();
        shape = dlg.getShape();
        ordercode = dlg.getOrdercode();
        price = dlg.getPrice();
    }
}

QList<Pin *> Component::getPins()
{
    Q_ASSERT_X(symbol, "getPins", "Component has no symbol.");
    return symbol->getPins();
}

// Helper function of load()
// Parse a single line by trying all regexes.
void Component::parseLine(const QString &line)
{
    QRegularExpressionMatch match;
    match = re_referance.match(line);
    if (match.hasMatch()) {
        reference = match.captured(1) + "?";
        return;
    }

    match = re_shape.match(line);
    if (match.hasMatch()) {
        shape = match.captured(1);
        return;
    }

    match = re_value.match(line);
    if (match.hasMatch()) {
        value = match.captured(1);
        return;
    }

    match = re_ordercode.match(line);
    if (match.hasMatch()) {
        ordercode = match.captured(1);
        return;
    }

    match = re_price.match(line);
    if (match.hasMatch()) {
        price = match.captured(1).toDouble();
        return;
    }

    match = re_sizes.match(line);
    if (match.hasMatch()) {
        int xsize = match.captured(1).toInt();
        int ysize = match.captured(2).toInt();
        symbol->setSize(QPoint(xsize, ysize));
        return;
    }

    match = re_vector.match(line);
    if (match.hasMatch()) {
        QPixmap* newpixmap = new QPixmap((int)(symbol->getSize().x() * QProjectView::GRID_SIZE + 1), (int)(symbol->getSize().y() * QProjectView::GRID_SIZE + 1));
        newpixmap->fill();
        symbol->setPixmap(newpixmap);
        return;
    }

    match = re_line.match(line);
    if (match.hasMatch()) {
        int x1 = (int)(QProjectView::GRID_SIZE * match.captured(1).toDouble());
        int y1 = (int)(QProjectView::GRID_SIZE * match.captured(2).toDouble());
        int x2 = (int)(QProjectView::GRID_SIZE * match.captured(3).toDouble());
        int y2 = (int)(QProjectView::GRID_SIZE * match.captured(4).toDouble());

        QPainter* painter = new QPainter(symbol->getPixmap());
        painter->setPen(COMP_COLOR);
        painter->drawLine(x1, y1, x2, y2);
        delete painter;
    }

    match = re_pin.match(line);
    if (match.hasMatch()) {
        QString position = match.captured(1);
        int number = match.captured(2).toInt();
        QString linetype = match.captured(3);
        QString electype = match.captured(4);
        QString name = match.captured(5);

        Pin::LineType lt = Pin::SHORT;
        if (linetype == "SHORT") lt = Pin::SHORT;
        else if (linetype == "NONE") lt = Pin::NONE;
        else qWarning() << "Illegal pin type: " <<  linetype;

        Pin::ElecType et = Pin::PASSIVE;
        if (electype == "PAS") et = Pin::PASSIVE;
        else if (electype == "IN") et = Pin::INPUT;
        else if (electype == "OUT") et = Pin::OUTPUT;
        else if (electype == "PWR") et = Pin::POWER;
        else if (electype == "IO") et = Pin::IO;
        else qWarning() << "Illegal electrical pin type: " << electype;

        symbol->addPin(new Pin(position, number, name, lt, et));
    }
}

// Calculate base point of the pin.
// Also return at which side of the component the pin is connected.
// Notice rotation and mirroring of symbol.
void Component::calculatePinPoint(const Pin &pin, Coordinates *base, Coordinates *end, Pin::Side *side)
{
    Q_ASSERT_X(pin.getPosition().size() > 1, "calculatePinPoint", "Invalid position. Too short.");

    // Get position on side
    int d = pin.getPosition().mid(1).toInt();

    // Determine side
    *side = getPinSide(pin);

    // Calculate base point
    switch (*side) {
    case Pin::TOP:
        if (mirrored != ((angle == 90) || (angle == 180))) d = getSize().x() - d;
        base->setX(lastPosition.x() + d);
        base->setY(lastPosition.y());
        break;
    case Pin::RIGHT:
        if ((angle == 180) || (angle == 270)) d = getSize().y() - d;
        base->setX(lastPosition.x() + getSize().x());
        base->setY(lastPosition.y() + d);
        break;
    case Pin::BOTTOM:
        if (mirrored != ((angle == 90) || (angle == 180))) d = getSize().x() - d;
        base->setX(lastPosition.x() + d);
        base->setY(lastPosition.y() + getSize().y());
        break;
    case Pin::LEFT:
        if ((angle == 180) || (angle == 270)) d = getSize().y() - d;
        base->setX(lastPosition.x());
        base->setY(lastPosition.y() + d);
        break;
    }

    *end = *base;
    switch (pin.getLinetype()) {
    case Pin::SHORT:
        switch(*side) {
        case Pin::TOP:
            end->setY(base->y() - 1);
            break;
        case Pin::BOTTOM:
            end->setY(base->y() + 1);
            break;
        case Pin::LEFT:
            end->setX(base->x() - 1);
            break;
        case Pin::RIGHT:
            end->setX(base->x() + 1);
            break;
        default:
            qCritical("Illegal pin position.");
            break;
        }
        break;
    case Pin::NONE:
        break;
    default:
        qWarning("Illegal pin type in %s.", qUtf8Printable(reference));
        break;
    }
}

QString Component::getReference() const
{
    return reference;
}

QString Component::getValue() const
{
    return value;
}

QString Component::getShape() const
{
    return shape;
}

Pin::Side Component::getPinSide(const Pin &pin)
{
    Pin::Side s;

    switch (pin.getPosition().at(0).toLatin1()) {
    case 'T':
        s = Pin::TOP;
        break;
    case 'R':
        s = Pin::RIGHT;
        break;
    case 'B':
        s = Pin::BOTTOM;
        break;
    case 'L':
        s = Pin::LEFT;
        break;
    default:
        qWarning("Invalid position %s.", qUtf8Printable(pin.getPosition()));
        s = Pin::TOP;
    }

    // Apply rotation
    s = static_cast<Pin::Side>(((s) + (angle / 90)) % 4);

    // Apply mirroring
    if (mirrored) {
        if (s == Pin::LEFT) s = Pin::RIGHT;
        else if (s == Pin::RIGHT) s = Pin::LEFT;
    }

    return s;
}

// Get size of symbol, while taking rotation into account.
QPoint Component::getSize() const
{
    if ((angle == 90) || (angle == 270)) {
        return QPoint(symbol->getSize().y(), symbol->getSize().x());
    } else {
        return symbol->getSize();
    }
}

void Component::placeRefAndValue()
{
    QSet<Pin::Side> occupied;
    foreach (Pin* pin, symbol->getPins()) {
        occupied.insert(getPinSide(*pin));
    }
    if (!occupied.contains(Pin::TOP) && !occupied.contains(Pin::BOTTOM)) {
        referenceDisplacement.setX(0.0);
        referenceDisplacement.setY(-1.0);
        valueDisplacement.setX(0.0);
        valueDisplacement.setY(getSize().y());
    } else if (!occupied.contains(Pin::RIGHT)) {
        referenceDisplacement.setX(getSize().x() + 0.5);
        referenceDisplacement.setY(0.0);
        valueDisplacement.setX(getSize().x() + 0.5);
        valueDisplacement.setY(1.0);
    } else if (!occupied.contains(Pin::BOTTOM)) {
        referenceDisplacement.setX(0.0);
        referenceDisplacement.setY(getSize().y() + 0.2);
        valueDisplacement.setX(0.0);
        valueDisplacement.setY(getSize().y() + 1.2);
    } else if (!occupied.contains(Pin::TOP)) {
        referenceDisplacement.setX(0.0);
        referenceDisplacement.setY(-2.0);
        valueDisplacement.setX(0.0);
        valueDisplacement.setY(-1.0);
    } else {
        referenceDisplacement.setX(getSize().x() + 0.5);
        referenceDisplacement.setY(getSize().y() + 0.2);
        valueDisplacement.setX(getSize().x() + 0.5);
        valueDisplacement.setY(getSize().y() + 1.2);
    }
}

bool Component::load(const QString& filename)
{
    QFileInfo fi(filename);
    QString symbolName = fi.fileName();

    symbol = Library::getInstance()->getSymbol(symbolName);
    if (symbol) {
        reference = symbol->getReference();
        value = symbol->getValue();
        shape = symbol->getShape();
        ordercode = symbol->getOrdercode();
        price = symbol->getPrice();

    } else {

        QFile file(filename);
        if(!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(0, "error", file.errorString());
            return false;
        }

        symbol = new Symbol();
        QTextStream in(&file);

        // Read lines
        while (!in.atEnd()) {
            parseLine(in.readLine());
        }

        file.close();

        symbol->setName(symbolName);
        symbol->setReference(reference);
        symbol->setValue(value);
        symbol->setShape(shape);
        symbol->setOrdercode(ordercode);
        symbol->setPrice(price);

        Library::getInstance()->addSymbol(symbol);
    }

    // set positions of reference and value
    placeRefAndValue();

    return true;
}

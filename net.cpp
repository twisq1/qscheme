#include "net.h"

Net::Net(Connection *connection)
{   
    connections.append(connection);
}

Net::~Net()
{
}

void Net::merge(Net *net)
{
    connections.append(net->connections);
}

void Net::clear()
{
    connections.clear();
}

bool Net::isEmpty()
{
    return connections.isEmpty();
}


void Net::draw(QProjectView *view)
{
    Q_UNUSED(view);
}

const QString Net::getClassName()
{
    return "net";
}

bool Net::isConnectedTo(Coordinates p)
{
    foreach (Connection* connection, connections) {
        if (connection->isConnectedTo(p)) return true;
    }
    return false;
}

QSet<Coordinates> Net::getCoordinates()
{
    QSet<Coordinates> list;
    foreach (Connection* connection, connections) {
        QSet<Coordinates> toadd = connection->getCoordinates();
        list.unite(toadd);
    }
    return list;
}

#include "qprojectview.h"
#include "junction.h"

Junction::Junction(Sheet* sheet)
    : Connection(sheet)
{

}

void Junction::draw(QProjectView *view)
{
    if (lastPosition.hasValue() && lastPosition.getOn_sheet(sheet)) {
        Coordinates topLeft(QPointF(lastPosition.x() - JUNCTION_WIDTH, lastPosition.y() - JUNCTION_WIDTH));
        Coordinates bottomRight(QPointF(lastPosition.x() + JUNCTION_WIDTH, lastPosition.y() + JUNCTION_WIDTH));
        view->drawEllipse(topLeft, bottomRight, 1, JUNCTION_COLOR, JUNCTION_COLOR);
    }
}

void Junction::put()
{
    sheet->addDrawable(this);
    sheet->setActiveDrawable(new Junction(*this));
}

bool Junction::isConnectedTo(Coordinates p)
{
    return (lastPosition == p);
}

QSet<Coordinates> Junction::getCoordinates()
{
    QSet<Coordinates> list;
    list.insert(lastPosition);
    return list;
}
